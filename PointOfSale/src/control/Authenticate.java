package control;

import dbconnection.DatabaseConnection;
import frame.Authentication;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class Authenticate extends DatabaseConnection{
    
    public Authenticate(){
        
    }
    
    public int checkRegisterId(String id){
        int check = 0;
        try {
            sql = "select employee_id from employee where employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = 1;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return check;
        }
    }
    
    public int checkCashierLogin(String id){
        int check = 0;
        try {
            sql = "select employee_id from employee join job_title on employee.job_title_id = job_title.job_title_id where job_title.job_title_id = 3 and employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = 1;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return check;
        }
    }
    
    public int checkLoginId(String id){
        int check = 0;
        try {
            sql = "select employee_id from account where employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = 1;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return check;
        }
    }
    
    public int getJobTitle(String id){
        int jobTitle = 0;
        try {
            sql = "select job_title_id from employee where employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                jobTitle = result.getInt("job_title_id");
            }
            return jobTitle;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return jobTitle;
        }
    }
    
    public int addAccount(String empId, String password){
        sql = "insert into account(employee_id, password) values('"+empId+"', '"+password+"')";
        int result = connectToDatabase2(sql);
        return result;
    }
    
    public String getPassword(String id){
        String pass = null;
        try {
            
            sql = "select password from account where employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                pass = result.getString("password");
            }
            return pass;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return pass;
        }
    }
    
    public String getEmployeeName(String id){
        String name = null;
        try {
            
            sql = "select concat(last_name, ', ', first_name, ' ', mid(middle_name, 1, 1)) as 'name' from employee where employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                name = result.getString("name");
            }
            return name;
        } catch (SQLException ex) {
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return name;
        }
    }
    
}
