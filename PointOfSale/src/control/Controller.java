package control;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXTable;

public class Controller {
    
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    public static final int UPDATE_SUPPLIER = 0;
    public static final int UPDATE_CATEGORY = 1;
    public static final int UPDATE_SUB_CATEGORY = 2;
    public static final int UPDATE_BRAND = 3;
    public static final int UPDATE_QUANTITY = 4;
    public static final int UPDATE_PURCHASE_DATE = 5;
    public static final int UPDATE_EXPIRATION_DATE = 6;
    
    public static final int UPDATE_DATE_ACTIVE = 0;
    public static final int UPDATE_DATE_EXPIRE = 1;
    public static final int UPDATE_DATE_START = 2;
    public static final int UPDATE_DATE_END = 3;
    
    public static final String ADD = "add";
    public static final String MINUS = "minus";
    
    private int editRow = -1, editCol = -1, date, curRewId;
    private String order, sort, curEmpId, curEmpVal, curProdCode, curProdVal, curSupId, curSupVal, curRewVal, curCustId, curCustVal, selId, selVal, operation;
    private boolean focus, showColumn, remove, redeem, couponActivate;
    private TextPrompts placeHolder;
    
    public Controller(){}
    
    public void switchToPanel(JPanel main, JPanel child){
        main.removeAll();
        main.add(child);
        main.revalidate();
        main.repaint();
    }
    
    public void placeHolder(String text, JTextField field) {
        placeHolder = new TextPrompts(text, field);
    }
    
    public void showList(JPopupMenu popUpList, JScrollPane scroll, JList list, JTextField field){
        scroll.setPreferredSize(new Dimension(field.getWidth(), list.getHeight()));
        popUpList.removeAll();
        popUpList.add(scroll);
        popUpList.show(field, 0, 0);
        scroll.requestFocusInWindow();
    }
    
    public void showList(JPopupMenu popUpList, ArrayList<String> values, JList list, JScrollPane scroll, JTextField field){
        String[] value = new String[values.size()];
            for(int i = 0; i < values.size(); i++){
                value[i] = values.get(i);
            }
            list.removeAll();
            list.setListData(value);
            list.setBackground(Color.WHITE);
            scroll.setPreferredSize(new Dimension(field.getWidth(), 200));
            popUpList.removeAll();
            popUpList.add(scroll);
            popUpList.show(field, 0, 0);
            scroll.requestFocusInWindow();
    }
    
    public int calcQty(String operation, int curVal, int valueInput){
        int quantity = 0;
        
        switch(operation){
            case ADD:
                quantity = curVal + valueInput;
                break;
            case MINUS:
                quantity = curVal - valueInput;
                break;
        }
        
        return quantity;
    }
    
    public void setTitle(JLabel title, String content){
        title.setText(content);
    }
    
    public void orderFieldBy(String order){
        this.order = order;
    }
    
    public String getOrderedField(){
        return order;
    }
    
    public void setDate(int date){
        this.date = date;
    }
    
    public int getDate(){
        return date;
    }
    
    public void checkColumn(JXTable table, JCheckBox checkBox, String columnName){
        System.out.println("Oh yeah");
        if(checkBox.isSelected()){
            table.getColumnExt(columnName).setVisible(false);
        }
        else{
            table.getColumnExt(columnName).setVisible(true);
        }
    }
    
    public void setOperation(String operation){
        this.operation = operation;
    }
    
    public String getOperation(){
        return operation;
    }
    
    public void sortFieldBy(String sort){
        this.sort = sort;
    }
    
    public String getSortedField(){
        return sort;
    }
    
    public void setRedeem(boolean redeem){
        this.redeem = redeem;
    }
    
    public boolean isRedeemed(){
        return redeem;
    }
    
    public void setRemove(boolean remove){
        this.remove = remove;
    }
    
    public boolean isRemoved(){
        return remove;
    }
    
    public void setCouponActivate(boolean couponActivate){
        this.couponActivate = couponActivate;
    }
    
    public boolean couponWasActivated(){
        return couponActivate;
    }
    
    public void setEditedRow(int editRow){
        this.editRow = editRow;
    }
    
    public int getEditedRow(){
        return editRow;
    }
    
    public void setEditedColumn(int editCol){
        this.editCol = editCol;
    }
    
    public int getEditedColumn(){
        return editCol;
    }
    
    public void setCurrentEmployeeId(String curEmpId){
        this.curEmpId = curEmpId;
    }
    
    public void setCurrentProductBarcode(String curProdCode){
        this.curProdCode = curProdCode;
    }
    
    public void setCurrentRewardValue(String curRewVal){
        this.curRewVal = curRewVal;
    }
    
    public String getCurrentRewardValue(){
        return curRewVal;
    }
    
    public void setCurrentRewardId(int curRewId){
        this.curRewId = curRewId;
    }
    
    public int getCurrentRewardId(){
        return curRewId;
    }
    
    public void setCurrentSupplierId(String curSupId){
        this.curSupId = curSupId;
    }
    
    public String getCurrentSupplierId(){
        return curSupId;
    }
     
    public void setCurrentCustomerId(String curCustId){
        this.curCustId = curCustId;
    }
    
    public String getCurrentCustomerId(){
        return curCustId;
    }
    
    public void setCurrentCustomerValue(String curCustVal){
        this.curCustVal = curCustVal;
    }
    
    public  String getCurrentCustomerValue(){
        return curCustVal;
    }
    
    public void setCurrentSupplierValue(String curSupVal){
        this.curSupVal = curSupVal;
    }
    
    public String getCurrentSupplierValue(){
        return curSupVal;
    }
    
    public String getCurrentProductBarcode(){
        return curProdCode;
    }
    
    public void setCurrentProductValue(String curProdVal){
        this.curProdVal = curProdVal;
    }
    
    public String getCurrentProductValue(){
        return curProdVal;
    }
    
    public String getCurrentEmployeeId(){
        return curEmpId;
    }
    
    public void setCurrentEmployeeValue(String curEmpVal){
        this.curEmpVal = curEmpVal;
    }
    
    public String getCurrentEmployeeValue(){
        return curEmpVal;
    }
    
    public void setSelectedId(String selId){
        this.selId = selId;
    }
    
    public String getSelectedId(){
        return selId;
    }
    
    public void setSelectedValue(String selVal){
        this.selVal = selVal;
    }
    
    public String getSelectedValue(){
        return selVal;
    }
    
}
