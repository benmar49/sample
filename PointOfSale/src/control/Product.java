package control;

public class Product {
    
    private String barcode, productName, purchaseDate, expiryDate, description;
    private int supplier, category, subcategory, brand, quantity;
    double price;
    
    public Product(){}
    
    public void setProductInformation(String barcode, String productName, int supplier, int category, int subcategory, int brand, double price, int quantity, String purchaseDate, String expiryDate, String description){
        this.barcode = barcode;
        this.productName = productName;
        this.supplier = supplier;
        this.category = category;
        this.subcategory = subcategory;
        this.brand = brand;
        this.price = price;
        this.quantity = quantity;
        this.purchaseDate = purchaseDate;
        this.expiryDate = expiryDate;
        this.description = description;
    }
    
    public void setBarcode(String barcode){
        this.barcode = barcode;
    }
    
    public void setProductName(String productName){
        this.productName = productName;
    }
    
    public void setPurchaseDate(String purchaseDate){
        this.purchaseDate = purchaseDate;
    }
    
    public void setExpirationDate(String expiryDate){
        this.expiryDate = expiryDate;
    }
    
    public void setDescription(String description){
        this.description = description;
    }
    
    public void setSupplier(int supplier){
        this.supplier = supplier;
    }
    
    public void setCategory(int category){
        this.category = category;
    }
    
    public void setSubCategory(int subcategory){
        this.subcategory = subcategory;
    }
    
    public void setBrand(int brand){
        this.brand = brand;
    }
    
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }
    
    public void setPrice(double price){
        this.price = price;
    }
    
    public String getBarcode(){
        return barcode;
    }
    
    public String getProductName(){
        return productName;
    }
    
    public String getPurchaseDate(){
        return purchaseDate;
    }
    
    public String getExpirationDate(){
        return expiryDate;
    }
    
    public String getDescription(){
        return description;
    }
    
    public int getSupplier(){
        return supplier;
    }
    
    public int getCategory(){
        return category;
    }
    
    public int getSubCategory(){
        return subcategory;
    }
    
    public int getBrand(){
        return brand;
    }
    
    public int getQuantity(){
        return quantity;
    }
    
    public double getPrice(){
        return price;
    }
}
