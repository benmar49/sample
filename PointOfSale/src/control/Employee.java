package control;

public class Employee {
    
    private String empId, lastName, firstName, middleName;
    private int jobTitle, gender;
    
    public Employee(){}
    
    public void setEmployeeInformation(String empId, String lastName, String firstName, String middleName, int jobTitle, int gender){
        this.empId = empId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.jobTitle = jobTitle;
        this.gender = gender;
    }
    
    public void setEmployeeId(String empId){
        this.empId = empId;
    }
    
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    
    public void setMiddleName(String middleName){
        this.middleName = middleName;
    }
    
    public void setJobTitle(int jobTitle){
        this.jobTitle = jobTitle;
    }
    
    public void setGender(int gender){
        this.gender = gender;
    }
    
    public String getEmployeeId(){
        return empId;
    }
    
    public String getLastName(){
        return lastName;
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getMiddleName(){
        return middleName;
    }
    
    public int getJobTitle(){
        return jobTitle;
    }
    
    public int getGender(){
        return gender;
    }
}
