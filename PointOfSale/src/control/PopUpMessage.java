package control;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

public class PopUpMessage extends JPopupMenu{
    
    public static final Color ERROR_TEXT_COLOR = new Color(255,0,0);
    public static final Color SUCCESS_TEXT_COLOR = new Color(0,153,51);
    
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    
    private final ImageIcon successIcon = new ImageIcon(getClass().getResource("/icons/check.png"));
    private final ImageIcon errorIcon = new ImageIcon(getClass().getResource("/icons/error.png"));
    
    private JLabel label;
    private JLabel message;
    private JPanel panel;
    private JPanel container;
    
    /**
     * Initialize popup message
     * @param label
     * @param message
     * @param panel
     * @param container 
     */
    public PopUpMessage(JLabel label, JLabel message, JPanel panel, JPanel container){
        this.label = label;
        this.message = message;
        this.panel = panel;
        this.container = container;
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
    }
    
    /**
     * Set popup message
     * @param label
     * @param message
     * @param panel
     * @param container 
     */
    public void setPopUpMessage(JLabel label, JLabel message, JPanel panel, JPanel container){
        this.label = label;
        this.message = message;
        this.panel = panel;
        this.container = container;
    }
    
    /**
     * Show popup message
     * @param msg a message statement
     * @param status check status if true or false
     */
    public void showMessage(String msg, int status){
        if(status == SUCCESS){
            message.setForeground(SUCCESS_TEXT_COLOR);
            label.setIcon(successIcon);
        }
        else{
            message.setForeground(ERROR_TEXT_COLOR);
            label.setIcon(errorIcon);
        }
        message.setText(msg);
        add(panel);
        show(container, container.getWidth(), container.getHeight());
    }
}
