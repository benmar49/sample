package actions;

import java.sql.SQLException;
import javax.swing.JComboBox;
import org.jdesktop.swingx.JXTable;

public interface CustomerServiceTask {
    
    
//    public double calculatePoints(double total);
//    
//    public void pointsVariable();
//    public ResultSet getRewards();
//    public ResultSet getRewardsAvail(String customerPoint);
//    public ResultSet getCoupons();
//    public ResultSet getPromos();
//    public ResultSet getCustomers();
//    public ResultSet getNewCustomers();
//    public ResultSet getCustomerPoints();
//    public ResultSet getPointTransactions(String custID);
//    public ResultSet getLastCustId();
//    public int getTotalCustomers();
//    public ResultSet specificPoint(String custID);
//    
//    public void addCustomers(String customerId,String lastName, String firstName, String middleName, String gender, String datereg, String contact, String address);
//    public void addRewards(String rewardName, String rewardItem, String rewDesc, String equivalentPoints);
//    public void addCoupons(String couponCode, String couponDescription, String dateStart, String dateEnd);
//    public void addPromos(String promoName,String promoDescription,String promoCategory, String promoDateStart, String promoDateEnd);
//    public void addPoints(double pointEarned);
//    public void addPointTransactions(String custId, String pointId, String rewardId, String equivalentPoints);
//    public void regPoints(String custId);
//   
//    public void updateRewards(String rewardID, String rewardName, String rewardItem, String rewDesc, String equivalentPoints);
//    public void updateCoupons(String couponID, String couponCode, String couponDescription, String dateStart, String dateEnd);
//    public void updatePromos(String promoID,String promoName,String promoDescription,String promoCategory, String promoDateStart, String promoDateEnd);
//    public void updatePointsVariable(String PointVarVal);
//    public void updatePoint(String pointId,String subtPoint);
//    public void updatePromos(String promoName,String promoDesc,String promoCategory,String promoDateStart,String promoDateEnd);
//    public void updateCustomers(String custID, String lastName, String firstName, String middleName);
//    
//    
//    public ResultSet searchCustomers(String customerName, int SelectedIndex);
//    public ResultSet searchRewards(String rewardName, int SelectedIndex);
//    public ResultSet searchCoupons(String couponName,int SelectedIndex);
//    public ResultSet searchPromos(String promoName, int SelectedIndex);
//    
//    public void removeCustomer(String customerID);
//    public void removeRewards(String rewardID);
//    public void removeCoupons(String couponID);
//    public void removePromos(String promoID);
//    
//    public void addLog(String username, String action, String timestamp );
//    public void updateLog(String username, String action, String idNum, String timestamp);
//    public void removeLog(String username, String action, String idNum, String timestamp);
//    
//    public ResultSet countCustomers();
//    public ResultSet countPromotions();
//    public ResultSet countRewards();
//    public ResultSet countCoupons();
//    
//    public String getPointVariable();
//    
//    public void loadBrand(JComboBox comboBox, String firstIndex, String selCat, String selSub);
//    public void loadSubCategory(JComboBox comboBox, String firstIndex, String selCat);
//    public void loadCategories(JComboBox comboBox, String firstIndex);
    public void loadProduct(JComboBox comboBox, String firstIndex);
    
    
    
    
    
    
    
    //Kaning naa sa ubos ang akong code nin basihi langn ing mga method nga add, load, ug update para sa coupon ug promo
  
    public void addCustomer(String customerId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException;
    public void loadCustomer(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateCustomer(String curCustId, String newCustId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException;
    public void updateCustomerId(String curCustId, String newCustId) throws SQLException;
    public void updateCustomerLastName(String custId, String lastName) throws SQLException;
    public void updateCustomerFirstName(String custId, String firstName) throws SQLException;
    public void updateCustomerMiddleName(String custId, String midName) throws SQLException;
    public void updateCustomerGender(String custId, int gender) throws SQLException;
    public void updateCustomerAddress(String custId, String address) throws SQLException;
    public void updateCustomerContactNumber(String custId, String contactNo) throws SQLException;
    public void updateCustomerPoints(String custId, double pointSpent, double curPoints) throws SQLException;
    public int getTotalCustomer() throws SQLException;
    public String getCustomerName(String id) throws SQLException;
    public double getCustomerPoints(String id) throws SQLException;
    public void searchCustomer(JXTable table, String fieldName, String value) throws SQLException;
    
    public void addPromo(String promoName, String promoDesc, String barcode, double discount, String promoDateStart, String promoDateEnd) throws SQLException;
    public void loadPromo(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updatePromo(int id, String promoName, String desc, String barcode, double discount, String promoDateStart, String promoDateEnd) throws SQLException;
    public void updatePromoName(int id, String promoName) throws SQLException;
    public void updatePromoDescription(int id, String promoDesc) throws SQLException;
    public void updatePromoBarcode(int id, String barcode) throws SQLException;
    public void updatePromoDiscount(int id, double discount) throws SQLException;
    public void updatePromoDateStarted(int id, String dateStarted) throws SQLException;
    public void updatePromoDateEnd(int id, String dateEnd) throws SQLException;
    public int getPromoId(String promoName) throws SQLException;
    public int getTotalPromo() throws SQLException;
    public void searchPromo(JXTable table, String fieldName, String value) throws SQLException;
    
    public void addCoupon(String couponCode, String couponDesc, String barcode, double discount, String couponActive, String couponExpire) throws SQLException;
    public void loadCoupons(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateCoupon(int id, String couponName, String desc, String barcode, double discount, String couponDateActive, String couponDateExpire) throws SQLException;
    public void updateCouponName(int id, String couponName) throws SQLException;
    public void updateCouponDescription(int id, String couponDesc) throws SQLException;
    public void updateCouponBarcode(int id, String barcode) throws SQLException;
    public void updateCouponDiscount(int id, double discount) throws SQLException;
    public void updateCouponDateStarted(int id, String dateStarted) throws SQLException;
    public void updateCouponDateEnd(int id, String dateEnd) throws SQLException;
    public int getCouponId(String couponCode) throws SQLException;
    public void searchCoupon(JXTable table, String fieldName, String value) throws SQLException;
    public int getTotalCoupon() throws SQLException;
    
    public void addReward(String rewardName, String rewardItem, String rewardDesc, double points) throws SQLException;
    public void loadReward(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateReward(int id, String rewardName, String rewardItem, String rewardDesc, double point) throws SQLException;
    public void updateRewardName(int id, String rewardName) throws SQLException;
    public void updateRewardItem(int id, String item) throws SQLException;
    public void updateRewardDescription(int id, String description) throws SQLException;
    public void updateRewardPoint(int id, double point) throws SQLException;
    public int getTotalReward() throws SQLException;
    public double getRewardPoints(String rewardName) throws SQLException;
    public void searchReward(JXTable table, String fieldName, String value) throws SQLException;
    
    public void addPoints(String customerId, double pointAmt, double overallPoints) throws SQLException;
    public void updatePoints(String curCustId, String newCustId) throws SQLException;
    
    public void loadRedeemableRewards(JXTable table, double points, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public int getTotalRedeemableRewards(double points) throws SQLException;
    
    public void addRewardTrans(String customerId, String rewardName, double pointSpent) throws SQLException;
    public void loadRewardTrans(JXTable table, String customerId, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public int getTotalRewardTrans(String customerId) throws SQLException;
    
    public void loadRecentlyAddedCustomer(JXTable table) throws SQLException;
    public void loadCustomersHighestPoints(JXTable table) throws SQLException;
}
