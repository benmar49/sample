package actions;

import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public interface CashierTask {
    
    public void setCustomerId(String customerId);
    public String getCustomerId();
    public String getCustomerName(String customerId);
    public double getCustomerPoints(String customerId);
    
    public void setInvoiceId(String invoiceId);
    public String getInvoiceId();
    
    public void setEmployeeId(String employeeId);
    public String getEmployeeId();
    public String getEmployeeName(String employeeId);
    
    public boolean checkBarcode(String barcode);
    public void searchProduct(JTable table, String prodName);
    
    public void setProductInfo(String barcode);
    public String getProductBarcode();
    public String getProductName();
    public String getProductBrandName();
    public String getProductDesc();
    public double getProductPrice();
    public int getProductQuantity();
    
    public void setSearch(boolean status);
    public boolean isSearch();
    
    public void setSelectedRow(int row);
    public int getSelectedRow();
    
    public void setSelectedColumn(int col);
    public int getSelectedColumn();
    
    public void saveOrder(String invoiceId, String barcode, String prodName, String brandName, double price, int qty, double subTotal);
    public void saveTransaction(String invoiceId, String cashierId, String customerId, double invoiceAmt, double cash, double change);
    public void pendingTransaction(String invoiceId, String cashierId, String customerId, double invoiceAmt);
    public void loadPendingList(JTable table, String cashierId);
    public void loadOrder(JTable table, String invoiceId);
    public int getTotalTransaction(String cashierId);
    
    public void updatePoint(String pointId, String addPoint, String overPoint);
    public String getCustPoint(String custId);
    public String getCustOverallPoint(String custId);
    public ResultSet checkCustId(String custId);
    
    public void deductProductQty(String barcode, int qty);
    
    public double discountItem(String barcode);
    public boolean checkBarcodePromo(String barcode);
    
    public boolean checkCouponCode(String coupCode);
    public double couponCode(String coupCode);
   
    
}
