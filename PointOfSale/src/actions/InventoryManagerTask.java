package actions;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JList;
import org.jdesktop.swingx.JXTable;

public interface InventoryManagerTask {
    
    public void loadRecentProducts(JXTable table) throws SQLException;
    public void loadRecentCategories(JXTable table) throws SQLException;
    
    public void addEmployee(String id, String lastName, String firstName, String middleName, int jobTitle, int gender) throws SQLException;
    public void updateEmployee(String curId, String newId, String lastName, String firstName, String middleName, int jobTitle, int gender) throws SQLException;
    public void updateEmployeeId(String curId, String newId) throws SQLException;
    public void updateLastName(String id, String lastName) throws SQLException;
    public void updateFirstName(String id, String firstName) throws SQLException;
    public void updateMiddleName(String id, String middleName) throws SQLException;
    public void updateJobTitle(String id, int jobTitle) throws SQLException;
    public void updateGender(String id, int gender) throws SQLException;
    public void loadEmployee(JXTable table, int job_title, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void searchEmployee(JXTable table, String fieldName, String value) throws SQLException;
    public int getTotalEmployees() throws SQLException;
    public int getTotalInventoryManager() throws SQLException;
    public int getTotalCustomerService() throws SQLException;
    public int getTotalCashier() throws SQLException;
    public int getTotalSupervisor() throws SQLException;
    public ArrayList<String> getEmployeeInformation(String empId) throws SQLException;
    
    public void addProduct(String barcode, String prodName, int supplier, int category, int subcategory, int brand, double price, int quantity, String purchDate, String expDate, String desc) throws SQLException;
    public void updateProduct(String curBarcode, String newBarcode, String prodName, int supplier, int category, int subcategory, int brand, double price, int quantity, String purchDate, String expDate, String desc) throws SQLException;
    public void updateProductBarcode(String curBarcode, String newBarcode) throws SQLException;
    public void updateProductName(String barcode, String prodName) throws  SQLException;
    public void updateProductSupplier(String barcode, int supplier) throws SQLException;
    public void updateProductCategory(String barcode, int category) throws SQLException;
    public void updateProductSubCategory(String barcode, int subcategory) throws SQLException;
    public void updateProductBrand(String barcode, int brand) throws SQLException;
    public void updateProductPrice(String barcode, double price) throws SQLException;
    public void updateProductQuantity(String barcode, int quantity) throws SQLException;
    public void updateProductPurchaseDate(String barcode, String purchDate) throws SQLException;
    public void updateProductExpirationDate(String barcode, String expDate) throws SQLException;
    public void updateProductDescription(String barcode, String desc) throws SQLException;
    public void loadProduct(JXTable table, int load, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void loadProduct(JXTable table, String fieldName, String order, int offset, int rowLimit, int brandId) throws SQLException;
    public ArrayList<String> loadSuppliers() throws SQLException;
    public ArrayList<String> loadCategories() throws SQLException;
    public ArrayList<String> loadSubCategories(int categoryId) throws SQLException;
    public ArrayList<String> loadBrands() throws SQLException;
    public void searchProduct(JXTable table, String fieldName, String value) throws SQLException;
    public String getProductCategory(String barcode) throws SQLException;
    public void getProductSubCategory(String barcode, int categoryId, String subCategoryName) throws SQLException;
    public void getProductBrand(String barcode, int subCategoryId, String brandName) throws SQLException;
    public int getSupplierId(String supName) throws SQLException;
    public int getCategoryId(String categoryName) throws SQLException;
    public int getSubCategoryId(String subCategoryName) throws SQLException;
    public int getSubCategoryId(int categoryId) throws SQLException;
    public String getSubCategoryName(int categoryId) throws SQLException;
    public int getBrandId(String brandName) throws SQLException;
    public int getBrandId(int subCategoryId) throws SQLException;
    public int getTotalProducts() throws SQLException;
    public int getTotalBrandProducts(int brandId) throws SQLException;
    public int getTotalInStockProducts() throws SQLException;
    public int getTotalOutOfStockProducts() throws SQLException;
    public void removeProduct(String barcode) throws SQLException;
    public ArrayList<String> getProductInformation(String barcode) throws SQLException;
    
    public void addCategory(String categoryName) throws SQLException;
    public void addSubCategory(String subCategoryName, int categoryId) throws SQLException;
    public void loadCategory(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void loadSubCategory(JXTable table, int categoryId, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateCategory(int id, String categoryName) throws SQLException;
    public int getTotalCategories() throws SQLException;
    public int getTotalSubCategories(int categoryId) throws SQLException;
    public void searchCategory(JXTable table, String value) throws SQLException;
    public void searchSubCategory(JXTable table, int categoryId, String value) throws SQLException;
    public void updateSubCategory(int subCategoryId, String subCatName) throws SQLException;
    public void removeCategory(int categoryId) throws SQLException;
    public void removeSubCategory(int subCategoryId) throws SQLException;
     
    public void addBrand(String brandName) throws SQLException;
    public void loadBrand(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateBrand(int id, String brandName) throws SQLException;
    public int getTotalBrands() throws SQLException;
    public void searchBrand(JXTable table, String value) throws SQLException;
    public void removeBrand(int brandId) throws SQLException;
    
    public void addSupplier(String supplierName, String address, String contactNo, String email) throws SQLException;
    public void loadSupplier(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void searchSupplier(JXTable table, String fieldName, String value) throws SQLException;
    public void updateSupplier(int id, String supplierName, String address, String contactNo, String email) throws SQLException;
    public void updateSupplierId(int curId, int newId) throws SQLException;
    public void updateSupplierName(int id, String supplierName) throws SQLException;
    public void updateSupplierAddress(int id, String supplierAddress) throws SQLException;
    public void updateSupplierContactNumber(int id, String contactNo) throws SQLException;
    public void updateSuppleirEmailAddress(int id, String emailAdd) throws SQLException;
    public void removeSupplier(int id) throws SQLException;
    public int getTotalSuppliers() throws SQLException;
    
    public ArrayList<String> loadList(String sql, String fieldName) throws SQLException;
    
    public void loadSupplier(JComboBox comboBox, String firstIndex) throws SQLException;
    public void loadCategory(JComboBox comboBox, String firstIndex) throws SQLException;
    public void loadSubCategory(JComboBox comboBox, int categoryId, String firstIndex) throws SQLException;
    public void loadBrand(JComboBox comboBox, String firstIndex) throws SQLException;
    
    public int getJobTitleId(String jobTitle) throws SQLException;
    public int getGenderID(String gender) throws SQLException;
    
    public int getTotalNumber(String sql, String fieldName) throws SQLException;
    public ArrayList<String> getInformation(String sql, int columnCount) throws SQLException;
}
