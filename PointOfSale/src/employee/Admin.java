package employee;

import actions.CustomerServiceTask;
import actions.InventoryManagerTask;
import control.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import net.proteanit.sql.DbUtils;
import org.jdesktop.swingx.JXTable;

public class Admin extends Employee implements InventoryManagerTask, CustomerServiceTask{
    
    Product product;
    
    public Admin(){}

    @Override
    public void loadRecentProducts(JXTable table) throws SQLException {
        sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_price as 'Product Price', product_quantity as 'Quantity' from product order by date_added desc limit 10";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }

    @Override
    public void loadRecentCategories(JXTable table) throws SQLException{
        sql = "select category_id as 'ID', category_name as 'Category Name' from category order by date_added desc limit 10";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }

    @Override
    public void addEmployee(String id, String lastName, String firstName, String middleName, int jobTitle, int gender) throws SQLException {
        sql = "insert into employee(employee_id, last_name, first_name, middle_name, job_title_id, gender_id) values('"+id+"', '"+lastName+"', '"+firstName+"', '"+middleName+"', '"+jobTitle+"', '"+gender+"')";
        connect2(sql);
    }
    
    @Override
    public void updateEmployee(String curId, String newId, String lastName, String firstName, String middleName, int jobTitle, int gender) throws SQLException {
        sql = "update employee set employee_id = '"+newId+"', last_name = '"+lastName+"', first_name = '"+firstName+"', middle_name = '"+middleName+"', job_title_id = '"+jobTitle+"', gender_id = '"+gender+"' where employee_id = '"+curId+"'";
        connect2(sql);
    }
    
    @Override
    public void updateEmployeeId(String curId, String newId) throws SQLException {
        System.out.println(curId+"    "+newId);
        sql = "update employee set employee_id = '"+newId+"' where employee_id = '"+curId+"'";
        connect2(sql);
    }

    @Override
    public void updateLastName(String id, String lastName) throws SQLException {
        sql = "update employee set last_name = '"+lastName+"' where employee_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateFirstName(String id, String firstName) throws SQLException {
        sql = "update employee set first_name = '"+firstName+"' where employee_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateMiddleName(String id, String middleName) throws SQLException {
        sql = "update employee set middle_name = '"+middleName+"' where employee_id = '"+id+"'";
        connect2(sql);
    }
    
    @Override
    public void updateJobTitle(String id, int jobTitle) throws SQLException {
        sql = "update employee set job_title_id = '"+jobTitle+"' where employee_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateGender(String id, int gender) throws SQLException {
        sql = "update employee set gender_id = '"+gender+"' where employee_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void loadEmployee(JXTable table, int jobTitle, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        switch(jobTitle){
            case 0:
                sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 1:
                sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id where job_title = 'Inventory Manager' order by employee."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 2:
                sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id where job_title = 'Customer Service' order by employee."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 3:
                sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id where job_title = 'Cashier' order by employee."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 4:
                sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id where job_title = 'Supervisor' order by employee."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
        }
//        DefaultTableModel model = (DefaultTableModel) DbUtils.resultSetToTableModel(connect1(sql));
//        Object[] data = new Object[model.getRowCount()]; 
//        for(int i = 0; i < model.getRowCount(); i++){
//            data[i] = (i + 1);
//        }
//        model.addColumn("ID", data);
//        table.setModel(model);
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }
    
    @Override
    public int getTotalEmployees() throws SQLException {
        sql = "select count(employee_id) as 'value' from employee";
        connection.close();
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalInventoryManager() throws SQLException {
        sql = "select count(employee_id) as 'value' from employee where job_title_id = 2";
        connection.close();
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalCustomerService() throws SQLException {
        sql = "select count(employee_id) as 'value' from employee where job_title_id = 1";
        connection.close();
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalCashier() throws SQLException {
        sql = "select count(employee_id) as 'value' from employee where job_title_id = 3";
        connection.close();
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalSupervisor() throws SQLException {
        sql = "select count(employee_id) as 'value' from employee where job_title_id = 4";
        connection.close();
        return getTotalNumber(sql, "value");
    }

    @Override
    public ArrayList<String> getEmployeeInformation(String empId) throws SQLException{
        sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee";
        connection.close();
        return getInformation(sql, 6);
    }
    
    @Override
    public void searchEmployee(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select employee_id as 'Employee ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', job_title as 'Job Title', gender as 'Gender' from employee join job_title on employee.job_title_id = job_title.job_title_id join gender on employee.gender_id = gender.gender_id where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }

    @Override
    public int getTotalProducts() throws SQLException {
        sql = "select count(product_barcode) as 'value' from product";
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalBrandProducts(int brandId) throws SQLException {
        sql = "select count(product_barcode) as 'value' from product where product_brand_id = '"+brandId+"'";
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalInStockProducts() throws SQLException {
        sql = "select count(product_barcode) as 'value' from product where product_quantity != 0";
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalOutOfStockProducts() throws SQLException {
        sql = "select count(product_barcode) as 'value' from product where product_quantity = 0";
        return getTotalNumber(sql, "value");
    }
    
     @Override
    public int getJobTitleId(String jobTitle) throws SQLException {
        sql = "select job_title_id from job_title where job_title = '"+jobTitle+"'";
        return getTotalNumber(sql, "job_title_id");
    }

    @Override
    public int getGenderID(String gender) throws SQLException {
        sql = "select gender_id from gender where gender = '"+gender+"'";
        return getTotalNumber(sql, "gender_id");
    }

    @Override
    public void addProduct(String barcode, String prodName, int supplier, int category, int subcategory, int brand, double price, int quantity, String purchDate, String expDate, String desc) throws SQLException {
        sql = "insert into product(product_barcode, product_name, product_supplier_id, product_category_id, product_sub_category_id, product_brand_id, product_price, product_quantity, product_purchase_date, product_expiry_date, product_description) values('"+barcode+"', '"+prodName+"', '"+supplier+"', '"+category+"', '"+subcategory+"', '"+brand+"', '"+price+"', '"+quantity+"', '"+purchDate+"', '"+expDate+"', '"+desc+"')";
        connect2(sql);
    }

    @Override
    public void updateProduct(String curBarcode, String newBarcode, String prodName, int supplier, int category, int subcategory, int brand, double price, int quantity, String purchDate, String expDate, String desc) throws SQLException {
        sql = "update product set product_barcode = '"+newBarcode+"', product_name = '"+prodName+"', product_supplier_id = '"+supplier+"', product_category_id = '"+category+"', product_sub_category_id = '"+subcategory+"', product_brand_id = '"+brand+"', product_price = '"+price+"', product_quantity = '"+quantity+"', product_purchase_date = '"+purchDate+"', product_expiry_date = '"+expDate+"', product_description = '"+desc+"' where product_barcode = '"+curBarcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductBarcode(String curBarcode, String newBarcode) throws SQLException {
        sql = "update product set product_barcode = '"+newBarcode+"' where product_barcode = '"+curBarcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductName(String barcode, String prodName) throws SQLException {
        sql = "update product set product_name = '"+prodName+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductSupplier(String barcode, int supplier) throws SQLException {
        sql = "update product set product_supplier_id = '"+supplier+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductCategory(String barcode, int category) throws SQLException {
        sql = "update product set product_category_id = '"+category+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductSubCategory(String barcode, int subcategory) throws SQLException {
        sql = "update product set product_sub_category_id = '"+subcategory+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductBrand(String barcode, int brand) throws SQLException {
        sql = "update product set product_brand_id = '"+brand+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductPrice(String barcode, double price) throws SQLException {
        sql = "update product set product_price = '"+price+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductQuantity(String barcode, int quantity) throws SQLException {
        sql = "update product set product_quantity = '"+quantity+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductPurchaseDate(String barcode, String purchDate) throws SQLException {
        sql = "update product set product_purchase_date = '"+purchDate+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductExpirationDate(String barcode, String expDate) throws SQLException {
        sql = "update product set product_expiry_date = '"+expDate+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void updateProductDescription(String barcode, String desc) throws SQLException {
        sql = "update product set product_description = '"+desc+"' where product_barcode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void loadProduct(JXTable table, int load, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        switch(load){
            //Date Added, Barcode, Product Name, Price, Quantity, Purchase Date, Expiration Date
            case 0:
                sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_description as 'Description', supplier_name as 'Supplier', category_name as 'Category', sub_category_name as 'Sub-category', brand_name as 'Brand', product_price as 'Price', product_quantity as 'Quantity', product_purchase_date as 'Purchase Date', product_expiry_date as 'Expiry Date' from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id order by product."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 1:
                sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_description as 'Description', supplier_name as 'Supplier', category_name as 'Category', sub_category_name as 'Sub-category', brand_name as 'Brand', product_price as 'Price', product_quantity as 'Quantity', product_purchase_date as 'Purchase Date', product_expiry_date as 'Expiry Date' from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where product_quantity != 0 order by product."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
                break;
            case 2:
                sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_description as 'Description', supplier_name as 'Supplier', category_name as 'Category', sub_category_name as 'Sub-category', brand_name as 'Brand', product_price as 'Price', product_quantity as 'Quantity', product_purchase_date as 'Purchase Date', product_expiry_date as 'Expiry Date' from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where product_quantity = 0 order by product."+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        }
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }

    @Override
    public void loadProduct(JXTable table, String fieldName, String order, int offset, int rowLimit, int brandId) throws SQLException {
        sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_quantity as 'Quantity' from product where product_brand_id = '"+brandId+"' order by product_name asc";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }

    @Override
    public void removeProduct(String barcode) throws SQLException {
        sql = "delete product where product_barocode = '"+barcode+"'";
        connect2(sql);
    }

    @Override
    public void searchProduct(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select product_barcode as 'Barcode', product_name as 'Product Name', product_description as 'Description', supplier_name as 'Supplier', category_name as 'Category', sub_category_name as 'Sub-category', brand_name as 'Brand', product_price as 'Price', product_quantity as 'Quantity', product_purchase_date as 'Purchase Date', product_expiry_date as 'Expiry Date' from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
        connection.close();
    }
    
    @Override
    public ArrayList<String> getProductInformation(String barcode) throws SQLException {
        sql = "select product_barcode as 'Barcode', product_name as 'Product Name', supplier_name as 'Supplier', category_name as 'Category', sub_category_name as 'Sub-category', brand_name as 'Brand', product_price as 'Price', product_quantity as 'Quantity', product_purchase_date as 'Purchase Date', product_expiry_date as 'Expiry Date', product_description as 'Description' from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where product_barcode = '"+barcode+"'";
        return getInformation(sql, 11);
    }

    @Override
    public String getProductCategory(String barcode) throws SQLException {
        String name = "";
        sql = "select category_name from product join category on product.product_category_id = category.category_id where product_barcode = '"+barcode+"'";
        result = connect1(sql);
        if(result.next()){
            name = result.getString("category_name");
        }
        return name;
    }

    @Override
    public void getProductSubCategory(String barcode, int categoryId, String subCategoryName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getProductBrand(String barcode, int subCategoryId, String brandName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCategory(String categoryName) throws SQLException {
        sql = "insert into category(category_name) values('"+categoryName+"')";
        connect2(sql);
    }

    @Override
    public void addSubCategory(String subCategoryName, int categoryId) throws SQLException {
        sql = "insert into sub_category(sub_category_name, category_id) values('"+subCategoryName+"', '"+categoryId+"')";
        connect2(sql);
    }

    @Override
    public void loadCategory(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select category_id as 'ID', category_name as 'Category Name' from category order by "+fieldName+" asc limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void loadSubCategory(JXTable table, int categoryId, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select sub_category_id as 'ID', sub_category_name as 'Sub-category Name' from sub_category where category_id = '"+categoryId+"' order by "+fieldName+" asc limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void searchCategory(JXTable table, String value) throws SQLException {
        sql = "select category_id as 'ID', category_name as 'Category Name' from category where category_name like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void searchSubCategory(JXTable table, int categoryId, String value) throws SQLException{
        sql = "select sub_category_id as 'ID', sub_category_name as 'Sub-category Name' from sub_category where category_id = '"+categoryId+"'loadB and sub_category_name like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void removeCategory(int categoryId) throws SQLException {
        sql = "delete from category where category_id = '"+categoryId+"'";
        connect2(sql);
    }

    @Override
    public void removeSubCategory(int subCategoryId) throws SQLException {
        sql = "delete from sub_category where sub_category_id = '"+subCategoryId+"'";
        connect2(sql);
    }

    @Override
    public void updateCategory(int id, String categoryName) throws SQLException {
        sql = "update category set category_name = '"+categoryName+"' where category_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateSubCategory(int subCategoryId, String subCatName) throws SQLException {
        sql = "update sub_category set sub_category_name = '"+subCatName+"' where sub_category_id = '"+subCategoryId+"'";
        connect2(sql);
    }

    @Override
    public int getTotalCategories() throws SQLException {
        sql = "select count(category_id) as 'value' from category";
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getTotalSubCategories(int categoryId) throws SQLException {
        sql = "select count(sub_category_id) as 'value' from sub_category where category_id = '"+categoryId+"'";
        return getTotalNumber(sql, "value");
    }
    
    @Override
    public int getSupplierId(String supName) throws SQLException {
        sql = "select supplier_id from supplier where supplier_name = '"+supName+"'";
        return getTotalNumber(sql, "supplier_id");
    }

    @Override
    public int getCategoryId(String categoryName) throws SQLException {
        sql = "select category_id from category where category_name = '"+categoryName+"'";
        return getTotalNumber(sql, "category_id");
    }

    @Override
    public int getSubCategoryId(String subCategoryName) throws SQLException {
        sql = "select sub_category_id from sub_category where sub_category_name = '"+subCategoryName+"'";
        return getTotalNumber(sql, "sub_category_id");
    }

    @Override
    public int getSubCategoryId(int categoryId) throws SQLException {
        sql = "select sub_category_id from sub_category where category_id = '"+categoryId+"'";
        return getTotalNumber(sql, "sub_category_id");
    }

    @Override
    public String getSubCategoryName(int categoryId) throws SQLException {
        String name = "";
        sql = "select sub_category_name from sub_category where category_id = '"+categoryId+"'";
        result = connect1(sql);
        if(result.next())
            name = result.getString("sub_category_name");
        
        return name;
    }

    @Override
    public int getBrandId(String brandName) throws SQLException {
        sql = "select brand_id from brand where brand_name = '"+brandName+"'";
        return getTotalNumber(sql, "brand_id");
    }

    @Override
    public int getBrandId(int subCategoryId) throws SQLException {
        sql = "select brand_id from brand where sub_category_id = '"+subCategoryId+"'";
        return getTotalNumber(sql, "brand_id");
    }

    @Override
    public void loadSupplier(JComboBox comboBox, String firstIndex) throws SQLException {
        sql = "select supplier_name from supplier";
        result = connect1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        while(result.next()){
            comboBox.addItem(result.getString("supplier_name"));
        }
        connection.close();
    }

    @Override
    public void loadBrand(JComboBox comboBox, String firstIndex) throws SQLException {
        sql = "select brand_name from brand";
        result = connect1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        while(result.next()){
            comboBox.addItem(result.getString("brand_name"));
        }
        connection.close();
    }

    @Override
    public void loadCategory(JComboBox comboBox, String firstIndex) throws SQLException {
        sql = "select category_name from category";
        result = connect1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        while(result.next()){
            comboBox.addItem(result.getString("category_name"));
        }
        connection.close();
    }

    @Override
    public void loadSubCategory(JComboBox comboBox, int categoryId, String firstIndex) throws SQLException {
        sql = "select sub_category_name from sub_category where category_id = '"+categoryId+"'";
        result = connect1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        while(result.next()){
            comboBox.addItem(result.getString("sub_category_name"));
        }
        connection.close();
    }

    @Override
    public ArrayList<String> loadSuppliers() throws SQLException {
        sql = "select supplier_name from supplier";
        return loadList(sql, "supplier_name");
    }

    @Override
    public ArrayList<String> loadBrands() throws SQLException {
        sql = "select brand_name from brand";
        return loadList(sql, "brand_name");
    }

    @Override
    public ArrayList<String> loadCategories() throws SQLException {
        sql = "select category_name from category";
        return loadList(sql, "category_name");
    }

    @Override
    public ArrayList<String> loadSubCategories(int categoryId) throws SQLException {
        sql = "select sub_category_name from sub_category where category_id = '"+categoryId+"'";
        return loadList(sql, "sub_category_name");
    }

    @Override
    public void addBrand(String brandName) throws SQLException {
        sql = "insert into brand(brand_name) values('"+brandName+"')";
        connect2(sql);
    }

    @Override
    public void updateBrand(int id, String brandName) throws SQLException {
        sql = "update brand set brand_name = '"+brandName+"' where brand_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void loadBrand(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select brand_id as 'ID', brand_name as 'Brand Name' from brand order by "+fieldName+" asc limit "+offset+", "+rowLimit+"";
        DefaultTableModel model = (DefaultTableModel) DbUtils.resultSetToTableModel(connect1(sql));
        table.setModel(model);
    }

    @Override
    public int getTotalBrands() throws SQLException {
        sql = "select count(brand_id) as 'value' from brand";
        return getTotalNumber(sql, "value");
    }

    @Override
    public void searchBrand(JXTable table, String value) throws SQLException {
        sql = "select brand_id as 'ID', brand_name as 'Brand Name' from brand where brand_name like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void removeBrand(int brandId) throws SQLException {
        sql = "delete from brand where brand_id = '"+brandId+"'";
        connect2(sql);
    }

    @Override
    public void addSupplier(String supplierName, String address, String contactNo, String email) throws SQLException {
        sql = "insert into supplier(supplier_name, supplier_address, supplier_contact_no, supplier_email) values('"+supplierName+"', '"+address+"', '"+contactNo+"', '"+email+"')";
        connect2(sql);
    }

    @Override
    public void loadSupplier(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select supplier_id as 'Supplier ID', supplier_name as 'Supplier Name', supplier_address as 'Supplier Address', supplier_contact_no as 'Contact Number', supplier_email as 'Email Address' from supplier order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void searchSupplier(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select supplier_name as 'Supplier Name', supplier_address as 'Supplier Address', supplier_contact_no as 'Contact Number', supplier_email as 'Email Address' from supplier where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void updateSupplier(int id, String supplierName, String address,  String contactNo, String email) throws SQLException {
        sql = "update supplier set supplier_name = '"+supplierName+"', supplier_address = '"+address+"', supplier_contact_no = '"+contactNo+"', supplier_email = '"+email+"' where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateSupplierId(int curId, int newId) throws SQLException {
        sql = "update supplier set supplier_id = '"+newId+"' where supplier_id = '"+curId+"'";
        connect2(sql);
    }

    @Override
    public void updateSuppleirEmailAddress(int id, String emailAdd) throws SQLException {
        sql = "update supplier set supplier_email = '"+emailAdd+"' where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateSupplierAddress(int id, String supplierAddress) throws SQLException {
        sql = "update supplier set supplier_address = '"+supplierAddress+"' where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateSupplierContactNumber(int id, String contactNo) throws SQLException {
        sql = "update supplier set supplier_contact_no = '"+contactNo+"' where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateSupplierName(int id, String supplierName) throws SQLException {
        sql = "update supplier set supplier_name = '"+supplierName+"' where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public int getTotalSuppliers() throws SQLException {
        sql = "select count(supplier_id) as 'value' from supplier";
        return getTotalNumber(sql, "value");
    }

    @Override
    public void removeSupplier(int id) throws SQLException {
        sql = "delete from supplier where supplier_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public ArrayList<String> loadList(String sql, String fieldName) throws SQLException {
        ArrayList<String> supplierName = new ArrayList<>();
        
        result = connect1(sql);
        
        while(result.next()){
            supplierName.add(result.getString(fieldName));
        }
        
        connection.close();
        return supplierName;
    }
    
    @Override
    public int getTotalNumber(String sql, String fieldName) throws SQLException {
        int value = 0;
        
        result = connect1(sql);
        if(result.next())
            value = result.getInt(fieldName);
        
        connection.close();
        return value;
    }

    @Override
    public ArrayList<String> getInformation(String sql, int columnCount) throws SQLException {
        ArrayList<String> info = new ArrayList<>();
        
        result = connect1(sql);
        while(result.next()){
            for(int i = 1; i <= columnCount; i++){
                info.add(result.getString(i));
            }
        }
        
        connection.close();
        return info;
    }

    @Override
    public void addCustomer(String customerId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException {
        sql = "insert into customers(customer_id, last_name, first_name, middle_name, gender, customer_address, customer_contact) values('"+customerId+"', '"+lastName+"', '"+firstName+"', '"+midName+"', '"+gender+"', '"+address+"', '"+contNo+"')";
        connect2(sql);
    }

    @Override
    public void loadCustomer(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select customers.customer_id as 'Customer ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', gender.gender as 'Gender', customer_address as 'Address', customer_contact as 'Contact #', point_amount as 'Points', overall_points as 'Overall Points' from customers join points on customers.customer_id = points.customer_id join gender on customers.gender = gender.gender_id order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void updateCustomer(String curCustId, String newCustId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException {
        sql = "update customers set customer_id = '"+newCustId+"', last_name = '"+lastName+"', first_name = '"+firstName+"', middle_name = '"+midName+"', gender = '"+gender+"', customer_address = '"+address+"', customer_contact = '"+contNo+"' where customer_id = '"+curCustId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerId(String curCustId, String newCustId) throws SQLException {
        sql = "update customers set customer_id = '"+newCustId+"' where customer_id = '"+curCustId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerLastName(String custId, String lastName) throws SQLException {
        sql = "update customers set last_name = '"+lastName+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerFirstName(String custId, String firstName) throws SQLException {
        sql = "update customers set first_name = '"+firstName+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerMiddleName(String custId, String midName) throws SQLException {
        sql = "update customers set middle_name = '"+midName+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerGender(String custId, int gender) throws SQLException {
        sql = "update customers set gender = '"+gender+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerAddress(String custId, String address) throws SQLException {
        sql = "update customers set customer_address = '"+address+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerContactNumber(String custId, String contactNo) throws SQLException {
        sql = "update customers set customer_contact = '"+contactNo+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public void updateCustomerPoints(String custId, double pointSpent, double curPoints) throws SQLException {
        double points = curPoints - pointSpent;
        sql = "update points set point_amount = '"+points+"' where customer_id = '"+custId+"'";
        connect2(sql);
    }

    @Override
    public int getTotalCustomer() throws SQLException {
        sql = "select count(customer_id) as 'value' from customers";
        return getTotalNumber(sql, "value");
    }

    @Override
    public String getCustomerName(String id) throws SQLException {
        String name = "";
        sql = "select concat(last_name, ', ', first_name, ' ', mid(middle_name,1 ,1)) as 'name' from customers where customer_id = '"+id+"'";
        result = connect1(sql);
        if(result.next())
            name = result.getString("name");
        return name;
    }

    @Override
    public double getCustomerPoints(String id) throws SQLException {
        double points = 0;
        sql = "select point_amount from points where customer_id = '"+id+"'";
        result = connect1(sql);
        if(result.next())
            points = result.getDouble("point_amount");
        return points;
    }

    @Override
    public void searchCustomer(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select customers.customer_id as 'Customer ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name', gender.gender as 'Gender', customer_address as 'Address', customer_contact as 'Contact #', point_amount as 'Points', overall_points as 'Overall Points' from customers join points on customers.customer_id = points.customer_id join gender on customers.gender = gender.gender_id where customers."+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void addPromo(String promoName, String promoDesc, String barcode, double discount, String promoDateStart, String promoDateEnd) throws SQLException {
        sql = "insert into promotions(promo_name, promo_description, product_id, discount_percent, promo_date_start, promo_date_end) values('"+promoName+"', '"+promoDesc+"', '"+barcode+"', '"+discount+"', '"+promoDateStart+"', '"+promoDateEnd+"')";
        connect2(sql);
    }

    @Override
    public void loadPromo(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select promo_name as 'Promo Name', promo_description as 'Promo Description', product_id as 'Product ID', discount_percent as 'Discount', promo_date_start as 'Date Start', promo_date_end as 'Date End' from promotions order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void updatePromo(int id, String promoName, String desc, String barcode, double discount, String promoDateStart, String promoDateEnd) throws SQLException {
        sql = "update promotions set promo_name = '"+promoName+"', promo_description = '"+desc+"', product_id = '"+barcode+"', discount_percent = '"+discount+"', promo_date_start = '"+promoDateStart+"', promo_date_end = '"+promoDateEnd+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoBarcode(int id, String barcode) throws SQLException {
        sql = "update promotions set barcode = '"+barcode+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoDateEnd(int id, String dateEnd) throws SQLException {
        sql = "update promotions set promo_date_end = '"+dateEnd+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoDateStarted(int id, String dateStarted) throws SQLException {
        sql = "update promotions set promo_date_start = '"+dateStarted+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoDescription(int id, String promoDesc) throws SQLException {
        sql = "update promotions set promo_description = '"+promoDesc+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoDiscount(int id, double discount) throws SQLException {
        sql = "update promotions set promo_discount = '"+discount+"' where promo_id = '"+discount+"'";
        connect2(sql);
    }

    @Override
    public void updatePromoName(int id, String promoName) throws SQLException {
        sql = "update promotions set promo_name = '"+promoName+"' where promo_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public int getTotalPromo() throws SQLException {
        sql = "select count(promo_id) as 'value' from promotions";
        return getTotalNumber(sql, "value");
    }

     @Override
    public void searchPromo(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select promo_name as 'Promo Name', promo_description as 'Promo Description', barcode as 'Barcode', promo_discount as 'Discount', promo_date_start as 'Date Start', promo_date_end as 'Date End' from promotions where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }
    
    @Override
    public int getPromoId(String promoName) throws SQLException {
        sql = "select promo_id from promotions where promo_name = '"+promoName+"'";
        return getTotalNumber(sql, "promo_id");
    }

    @Override
    public void addCoupon(String couponCode, String couponDesc, String barcode, double discount, String couponActive, String couponExpire) throws SQLException {
        sql = "insert into coupons(coupon_code, coupon_description, product_id, discount_percent, date_active, date_expire) values('"+couponCode+"', '"+couponDesc+"', '"+barcode+"', '"+discount+"', '"+couponActive+"', '"+couponExpire+"')";
        connect2(sql);
    }
    
    @Override
    public void loadCoupons(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select coupon_code as 'Coupon Code', coupon_description as 'Coupon Description', product_id as 'Product ID', discount_percent as 'Discount', date_active as 'Date Active', date_expire as 'Date Expired' from coupons order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void updateCoupon(int id, String couponName, String desc, String barcode, double discount, String couponDateActive, String couponDateExpire) throws SQLException {
        sql = "update coupons set coupon_code = '"+couponName+"', coupon_description = '"+desc+"', product_id = '"+barcode+"', discount_percent = '"+discount+"', date_active = '"+couponDateActive+"', date_expire = '"+couponDateExpire+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponBarcode(int id, String barcode) throws SQLException {
        sql = "update coupons set barcode = '"+barcode+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponDateEnd(int id, String dateEnd) throws SQLException {
        sql = "update coupons set date_expire = '"+dateEnd+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponDateStarted(int id, String dateStarted) throws SQLException {
        sql = "update coupons set date_active = '"+dateStarted+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponDescription(int id, String couponDesc) throws SQLException {
        sql = "update coupons set coupon_description = '"+couponDesc+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponDiscount(int id, double discount) throws SQLException {
        sql = "update coupons set coupon_discount = '"+discount+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateCouponName(int id, String couponName) throws SQLException {
        sql = "update coupons set coupon_code = '"+couponName+"' where coupon_id = '"+id+"'";
        connect2(sql);
    }
    
    @Override
    public void searchCoupon(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select coupon_code as 'Coupon Code', coupon_description as 'Coupon Description', barcode as 'Barcode', coupon_discount as 'Discount', date_active as 'Date Active', date_expire as 'Date Expired' from coupons where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public int getTotalCoupon() throws SQLException {
        sql = "select count(coupon_id) as 'value' from coupons";
        return getTotalNumber(sql, "value");
    }

    @Override
    public int getCouponId(String couponName) throws SQLException {
        sql = "select coupon_id from coupons where coupon_code = '"+couponName+"'";
        return getTotalNumber(sql, "coupon_id");
    }

    @Override
    public void addReward(String rewardName, String rewardItem, String rewardDesc, double points) throws SQLException {
        sql = "insert into rewards(reward_name, reward_item, reward_description, equivalent_points) values('"+rewardName+"', '"+rewardItem+"', '"+rewardDesc+"', '"+points+"')";
        connect2(sql);
    }
    
    @Override
    public void loadReward(JXTable table, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select reward_id as 'ID', reward_name as 'Reward Name', reward_item as 'Reward Item', reward_description as 'Description', equivalent_points as 'Equivalent Points' from rewards order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void updateReward(int id, String rewardName, String rewardItem, String rewardDesc, double point) throws SQLException {
        sql = "update rewards set reward_name = '"+rewardName+"', reward_item = '"+rewardItem+"', reward_description = '"+rewardDesc+"', equivalent_points = '"+point+"' where reward_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateRewardName(int id, String rewardName) throws SQLException {
        sql = "update rewards set reward_name = '"+rewardName+"' where reward_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateRewardItem(int id, String item) throws SQLException {
        sql = "update rewards set reward_item = '"+item+"' where reward_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateRewardDescription(int id, String description) throws SQLException {
        sql = "update rewards set reward_description = '"+description+"' where reward_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public void updateRewardPoint(int id, double point) throws SQLException {
        sql = "update rewards set equivalent_points = '"+point+"' where reward_id = '"+id+"'";
        connect2(sql);
    }

    @Override
    public int getTotalReward() throws SQLException {
        sql = "select count(reward_id) as 'value' from rewards";
        return getTotalNumber(sql, "value");
    }

    @Override
    public double getRewardPoints(String rewardName) throws SQLException {
        double point = 0;
        sql = "select equivalent_points from rewards where reward_id = '"+rewardName+"'";
        result = connect1(sql);
        if(result.next())
            point = result.getDouble("equivalent_points");
        return point;
    }

    @Override
    public void searchReward(JXTable table, String fieldName, String value) throws SQLException {
        sql = "select reward_id as 'ID', reward_name as 'Reward Name', reward_item as 'Reward Item', reward_description as 'Description', equivalent_points as 'Equivalent Points' from rewards where "+fieldName+" like '%"+value+"%'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }
    
    @Override
    public void addPoints(String customerId, double pointAmt, double overallPoints) throws SQLException {
        sql = "insert into points(customer_id, point_amount, overall_points) values('"+customerId+"', '"+pointAmt+"', '"+overallPoints+"')";
        connect2(sql);
    }

    @Override
    public void updatePoints(String curCustId, String newCustId) throws SQLException {
        sql = "update points set customer_id = '"+newCustId+"' where customer_id = '"+curCustId+"'";
        connect2(sql);
    }

    @Override
    public void loadRedeemableRewards(JXTable table, double points, String order, String fieldName, int offset, int rowLimit) throws SQLException {
        sql = "select reward_id as 'ID', reward_name as 'Reward Name', reward_item as 'Reward Item', reward_description as 'Description', equivalent_points as 'Equivalent Points' from rewards where equivalent_points <= '"+points+"' order by "+fieldName+" "+order+" limit "+offset+", "+rowLimit+"";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public int getTotalRedeemableRewards(double points) throws SQLException {
        sql = "select count(reward_id) as 'value' from rewards where equivalent_points <= '"+points+"'";
        return getTotalNumber(sql, "value");
    }

    @Override
    public void addRewardTrans(String customerId, String rewardName, double pointSpent) throws SQLException {
        sql = "insert into reward_transaction(customer_id, reward_name, point_spent) values('"+customerId+"', '"+rewardName+"', '"+pointSpent+"')";
        connect2(sql);
    }

    @Override
    public void loadRewardTrans(JXTable table, String customerId, String fieldName, String order, int offset, int rowLimit) throws SQLException {
        sql = "select reward_transaction_id as 'ID', reward_name as 'Reward Name', point_spent as 'Point Spent', date_of_transaction as 'Date of Transaction' from reward_transaction where customer_id = '"+customerId+"'";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public int getTotalRewardTrans(String customerId) throws SQLException {
        sql = "select count(reward_name) as 'value' from reward_transaction where customer_id = '"+customerId+"'";
        return getTotalNumber(sql, "value");
    }

    @Override
    public void loadCustomersHighestPoints(JXTable table) throws SQLException {
        sql = " select customers.customer_id as 'Customer ID', concat(last_name, ', ', first_name, ' ', mid(middle_name, 1, 1), '.') as 'Name', gender.gender as 'Gender', point_amount as 'Points' from customers join points on customers.customer_id = points.customer_id join gender on customers.gender = gender.gender_id order by point_amount desc limit 0, 10";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }

    @Override
    public void loadRecentlyAddedCustomer(JXTable table) throws SQLException {
        sql = " select customers.customer_id as 'Customer ID', concat(last_name, ', ', first_name, ' ', mid(middle_name, 1, 1), '.') as 'Name', gender.gender as 'Gender' from customers join points on customers.customer_id = points.customer_id join gender on customers.gender = gender.gender_id limit 0, 10";
        table.setModel(DbUtils.resultSetToTableModel(connect1(sql)));
    }
    
    @Override
    public void loadProduct(JComboBox comboBox, String firstIndex) {
        sql = "select product_name from product";
                
                
        result = connectToDatabase1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        try{
        while(result.next()){
            comboBox.addItem(result.getString("product_name"));
        }
        connection.close();
        }catch(Exception E)
        {
            
        }
    }
}
