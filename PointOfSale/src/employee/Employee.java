package employee;

import dbconnection.DatabaseConnection;
import customtable.CustomTableModel;
import java.awt.Color;

public class Employee extends DatabaseConnection {
    
    public static final Color PRIMARY_COLOR = new Color(40,90,90);
    public static final Color SECONDARY_COLOR = new Color(25,76,76);
    public static final Color SUB_BTN_PRIMARY_COLOR = new Color(0,40,40);
    public static final Color SUB_BTN_SECONDARY_COLOR = new Color(0,30,30);
    public static final Color BUTTON_DISABLE_COLOR = new Color(204,204,204);
    public static final Color BUTTON_ENABLE_COLOR = SECONDARY_COLOR;
    
    public static final int SUCCESSFUL = 0;
    public static final int FAIL = 1;
    
    public Employee(){
        
    }
}
