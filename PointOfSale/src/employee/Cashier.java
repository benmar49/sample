package employee;

import actions.CashierTask;
import control.Authenticate;
import customtable.CustomTableModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import net.proteanit.sql.DbUtils;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class Cashier extends Employee implements CashierTask{

    private Authenticate auth = new Authenticate();
    private StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
    private String customerId;
    private String invoiceId;
    private String employeeId;
    private String customerIdRet;
    private String pointAmount;
    private String overallPointAmount;
    private String bCode;
    private String discPercentage;
    private String coupDiscPercentage;
    
    private String coupId;
    
    
    private String barcode;
    private String prodName;
    private String brandName;
    private String description;
    private double price,discprice,discountedPrice;
    private double coupPrice, coupDiscPrice,coupDiscountedPrice;
    private int qty;
    
    private int row;
    private int col;
    
    private boolean search = true;

    @Override
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
    @Override
    public String getCustomerId() {
        return customerId;
    }

    @Override
    public String getCustomerName(String customerId) {
        try {
            String name = "";
            sql = "select concat(last_name, ', ', first_name, ' ', mid(middle_name, 1, 1), '.') as 'name' from customers where customer_id = '"+customerId+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                name = result.getString("name");
            }
            return name;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public double getCustomerPoints(String customerId) {
        try {
            double points = 0;
            sql = "select overall_points from points where customer_id = '"+customerId+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                points = result.getDouble("overall_points");
            }
            return points;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    @Override
    public String getInvoiceId() {
        return invoiceId;
    }

    @Override
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String getEmployeeId() {
        return employeeId;
    }

    @Override
    public String getEmployeeName(String employeeId) {
        String name = "";
        try {
            sql = "select concat(last_name, ', ', first_name, ' ', mid(middle_name, 1, 1), '.') as 'name' where employee_id = '"+employeeId+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                name = result.getString("name");
            }
            return name;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public boolean checkBarcode(String barcode) {
        try {
            boolean check = false;
            sql = "select product_barcode from product where product_barcode = '"+barcode+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = true;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public void searchProduct(JTable table, String data) {
        try {
            CustomTableModel.clearTableRows(CustomTableModel.productTransTableModel);
            table.setModel(CustomTableModel.productTransTableModel);
            sql = "select * from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where product_barcode like '%"+data+"%' || product_name like '%"+data+"%'";
           
            result = connectToDatabase1(sql);
            while(result.next()){
                CustomTableModel.productTransTableModel.addRow(new Object[]{result.getString("product_barcode"), result.getString("product_name"), result.getString("brand_name"), "P"+result.getString("product_price"), result.getString("product_quantity")});
            }
            result.close();
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void setSearch(boolean status) {
        search = status;
    }

    @Override
    public boolean isSearch() {
        return search;
    }

    @Override
    public void setSelectedColumn(int col) {
        this.col = col;
    }

    @Override
    public int getSelectedColumn() {
        return col;
    }

    @Override
    public void setSelectedRow(int row) {
        this.row = row;
    }

    @Override
    public int getSelectedRow() {
        return row;
    }

    @Override
    public void setProductInfo(String barcode) {
        try {
            sql = "select product_barcode, product_name, product_description, supplier_name, category_name, sub_category_name, brand_name, product_price, product_quantity, product_purchase_date, product_expiry_date, product.date_added from product join supplier on product.product_supplier_id = supplier.supplier_id join category on product.product_category_id = category.category_id join sub_category on product.product_sub_category_id = sub_category.sub_category_id join brand on product.product_brand_id = brand.brand_id where product_barcode = '"+barcode+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                this.barcode = result.getString("product_barcode");
                this.prodName = result.getString("product_name");
                this.brandName = result.getString("brand_name");
                this.description = result.getString("product_description");
                this.price = result.getDouble("product_price");
                this.qty = result.getInt("product_quantity");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getProductBarcode() {
        return barcode;
    }

    @Override
    public String getProductName() {
        return prodName;
    }

    @Override
    public String getProductBrandName() {
        return brandName;
    }

    @Override
    public String getProductDesc() {
        return description;
    }
    
    @Override
    public double getProductPrice() {
        return price;
    }

    @Override
    public int getProductQuantity() {
        return qty;
    }

    @Override
    public void saveOrder(String invoiceId, String barcode, String prodName, String brandName, double price, int qty, double subTotal) {
        sql = "insert into orders(invoice_id, barcode, item_name, brand_name, price, quantity, subtotal) value('"+invoiceId+"', '"+barcode+"', '"+prodName+"', '"+brandName+"', '"+price+"', '"+qty+"', '"+subTotal+"')";
        connectToDatabase2(sql);
    }

    @Override
    public void saveTransaction(String invoiceId, String cashierId, String customerId, double invoiceAmt, double cash, double change) {
        sql = "insert into transactions(invoice_id, cashier_id, customer_id, invoice_amount, cash, cash_change) values('"+invoiceId+"', '"+cashierId+"', '"+customerId+"', '"+invoiceAmt+"', '"+cash+"', '"+change+"')";
        connectToDatabase2(sql);
    }

    @Override
    public void pendingTransaction(String invoiceId, String cashierId, String customerId, double invoiceAmt) {
        sql = "insert into pending_transaction(invoice_id, cashier_id, customer_id, invoice_amount) values('"+invoiceId+"', '"+cashierId+"', '"+customerId+"', '"+invoiceAmt+"')";
        connectToDatabase2(sql);
    }

    @Override
    public void loadPendingList(JTable table, String cashierId) {
        try {
            CustomTableModel.clearTableRows(CustomTableModel.pendingTableModel);
            table.setModel(CustomTableModel.pendingTableModel);
            sql = "select * from pending_transaction where cashier_id = '"+cashierId+"'";
            result = connectToDatabase1(sql);
            int iterate = 0;
            while(result.next()){
                iterate++;
                CustomTableModel.pendingTableModel.addRow(new Object[]{iterate, result.getString("invoice_id"), result.getString("cashier_id"), result.getString("customer_id"), "P"+result.getString("invoice_amount")});
            }
            result.close();
            table.getColumn(" ").setPreferredWidth(50);
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadOrder(JTable table, String invoiceId) {
        try {
            sql = "select * from orders where invoice_id = '"+invoiceId+"'";
            result = connectToDatabase1(sql);
            int iterate = 0;
            while(result.next()){
                iterate++;
                CustomTableModel.transactionTableModel.addRow(new Object[]{iterate, result.getString("barcode"), result.getString("item_name"), result.getString("brand_name"), "P"+result.getDouble("price"), result.getInt("quantity"), result.getDouble("subtotal")});
            }
            result.close();
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public int getTotalTransaction(String cashierId) {
        try {
            int total = 0;
            sql = "select count(invoice_id) as 'value' from transactions where cashier_id = '"+cashierId+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                total = result.getInt("value");
            }
            return total;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public void updatePoint(String pointId, String addPoint, String overPoint){
        sql = "update points set point_amount= '"+addPoint+"', overall_points= '"+overPoint+"'"
                + " where customer_id = '"+pointId+"'";
        connectToDatabase2(sql);
    } 

    @Override
    public String getCustPoint(String custId) {
        
        try{        
            sql = "select point_amount from points where customer_id = '"+custId+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            pointAmount  = result.getString("point_amount");   
            }
            result.close();
            return pointAmount;
            
        } 
        catch (SQLException ex) {
            return null;
        }
        
    }
    
     @Override
    public String getCustOverallPoint(String custId) {
        
        try{        
            sql = "select overall_points from points where customer_id = '"+custId+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            overallPointAmount  = result.getString("overall_points");   
            }
            result.close();
            return overallPointAmount;
            
        } 
        catch (SQLException ex) {
            return null;
        } 
        
    }

    @Override
    public ResultSet checkCustId(String custId) {
        try{
            connection = ConnectDb();
            PreparedStatement ps = connection.prepareStatement(
            "select * from customers"
                + " WHERE customer_id =?");
                ps.setObject(1, custId);
                result = ps.executeQuery(); 
                return result;
                }
         catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    @Override
    public void deductProductQty(String barcode, int qty) {
        String sql = "update product set product_quantity = product_quantity - "+qty+" where product_barcode = '"+barcode+"'";
        connectToDatabase2(sql);
    }
    
    public String getSupervisorPinNumber(String id){
        String pinNumber = null;
        try{
            sql = "SELECT password FROM account WHERE employee_id = '"+id+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                pinNumber = result.getString("password");
            }
            return pinNumber;
        }catch(SQLException ex){
            Logger.getLogger(Authenticate.class.getName()).log(Level.SEVERE, null, ex);
            return pinNumber;
        } 
    }
    
    @Override
    public double discountItem(String barcode){
        try{        
            sql = "select id from product where product_barcode = '"+barcode+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            bCode  = result.getString("id");
              
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
        try{        
            sql = "select product_price from product where id = '"+bCode+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            discprice  = Double.parseDouble(result.getString("product_price"));
           
                         
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
        try{        
            sql = "select discount_percent from promotions where product_id = '"+bCode+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            discPercentage  = result.getString("discount_percent");
                  
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
            double rebate = discprice*Double.parseDouble(discPercentage);
            discountedPrice = discprice-rebate;
            
            return discountedPrice;

    }
    @Override
    public boolean checkBarcodePromo(String barcode){
        
        try{        
            sql = "select id from product where product_barcode = '"+barcode+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            bCode  = result.getString("id");
            System.out.print(bCode);
            }
            result.close();
            
        } 
        catch (SQLException ex){
            
        }
        try {
            boolean check = false;
            sql = "select product_id from promotions where product_id = '"+bCode+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = true;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
       
    }

    @Override
    public boolean checkCouponCode(String coupCode) {
        try {
            boolean check = false;
            sql = "select coupon_code from coupons where coupon_code = '"+coupCode+"'";
            result = connectToDatabase1(sql);
            if(result.next()){
                check = true;
            }
            return check;
        } catch (SQLException ex) {
            Logger.getLogger(Cashier.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }

    @Override
    public double couponCode(String coupCode) {
        try{        
            sql = "select id from product where product_barcode = '"+coupCode+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            coupId  = result.getString("id");
            
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
        try{        
            sql = "select product_price from product where id = '"+coupId+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            coupPrice  = Double.parseDouble(result.getString("product_price"));
            
                         
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
        try{        
            sql = "select discount_percent from coupons where product_id = '"+coupId+"'";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            coupDiscPercentage  = result.getString("discount_percent");
             
            }
            result.close();
            
        } 
        catch (SQLException ex) {
            
        }
            double rebate1 = coupPrice*Double.parseDouble(coupDiscPercentage);
            coupDiscountedPrice = coupPrice-rebate1;
            
            return coupDiscountedPrice;

    }
}
