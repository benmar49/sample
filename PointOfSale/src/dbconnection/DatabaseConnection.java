package dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class DatabaseConnection{
    
    /**
    * Declares variables for database connection 
    */
    protected Connection connection;
    protected PreparedStatement statement;
    protected ResultSet result;
    protected String sql;
    protected boolean success;
    
    /**
    * Creates a connection from database
    */
    public static Connection ConnectDb(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/point_of_sale", "root", "");
            return con;
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
           return null;
        }
    }
    
    /**
     * 
     * @param sql an sql's statement
     * @return the result of a query
     */
    public ResultSet connectToDatabase1(String sql){
        try {
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            return result;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }
    
    /**
     * @param sql an sql's statement
     */
    public int connectToDatabase2(String sql){
        try {
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            statement.execute();
            connection.close();
            return 0;
        } catch (SQLException ex) {
            System.out.println(ex);
            return 1;
        }
    }
    
    public ResultSet connect1(String sql) throws SQLException{
        connection = ConnectDb();
        statement = connection.prepareStatement(sql);
        result = statement.executeQuery();
        return result;
    }
    
    public void connect2(String sql) throws SQLException{
        connection = ConnectDb();
        statement = connection.prepareStatement(sql);
        statement.execute();
        connection.close();
    }   
    
    public void connectToDatabase3(String sql, String colName, JComboBox comboBox){
        try {
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            
            while(result.next()){
                comboBox.addItem(result.getString(colName));
            }
            
            result.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
