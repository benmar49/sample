package reports;

import dbconnection.DatabaseConnection;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;

public class Reports{
    
    public Reports(){
        
    }
    
    public JasperPrint printOrders(String invoiceId){
        JasperPrint jasperPrint;
        
        try {
            InputStream inputx = new FileInputStream(new File("C:/Users/marivic/Desktop/Projectl/PointOfSale/src/reports/orders.jrxml"));   
            JasperDesign jasperDesign = JRXmlLoader.load(inputx);                   
            
            JRDesignQuery jrd = new JRDesignQuery();
            String sql = "select orders.order_id, orders.invoice_id, item_name, price, quantity, subtotal, invoice_amount, cash, cash_change from orders join transactions on orders.invoice_id = transactions.invoice_id where orders.invoice_id = '"+invoiceId+"'";
            jrd.setText(sql);
            jasperDesign.setQuery(jrd);
            
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);            
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, DatabaseConnection.ConnectDb());
            
            return jasperPrint;
                                    
        } catch (JRException e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        } catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null, "File is missing!!!");
            return null;
        }
       
    }
}
