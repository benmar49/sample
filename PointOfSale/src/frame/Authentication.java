package frame;

import control.Authenticate;
import control.PopUpMessage;
import employee.Admin;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class Authentication extends javax.swing.JFrame {

    Admin adm = new Admin();
    Authenticate auth = new Authenticate();
    CustomerServiceFrame customerService = new CustomerServiceFrame();
    InventoryFrame inventory = new InventoryFrame();
    
    AdminFrame admin = new AdminFrame();
    StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    
    PopUpMessage popUp;
    
    public Authentication() {
        initComponents();
        popUp = new PopUpMessage(promptIcon, promptMsg, prompt, main);
        System.out.println(passwordEncryptor.encryptPassword("admin"));
        setLocationRelativeTo(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        prompt = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        promptIcon = new javax.swing.JLabel();
        promptMsg = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        Adverticement = new javax.swing.JPanel();
        main = new javax.swing.JPanel();
        login = new javax.swing.JPanel();
        loginLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        userName = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        passwordLabel = new javax.swing.JLabel();
        userPassword = new javax.swing.JPasswordField();
        jSeparator5 = new javax.swing.JSeparator();
        LoginBtn = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        message = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        RegistrationPanel = new javax.swing.JPanel();
        RegLabel = new javax.swing.JLabel();
        RegIDLabel = new javax.swing.JLabel();
        regID = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        passLabel = new javax.swing.JLabel();
        userPass = new javax.swing.JPasswordField();
        jSeparator9 = new javax.swing.JSeparator();
        reEnterPassLabel = new javax.swing.JLabel();
        reEnterPass = new javax.swing.JPasswordField();
        jSeparator6 = new javax.swing.JSeparator();
        RegBtn = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        message1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        prompt.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        promptIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/check.png"))); // NOI18N

        promptMsg.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        promptMsg.setForeground(new java.awt.Color(0, 153, 51));
        promptMsg.setText("PROMPT_MESSAGE");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(promptIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(promptMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(promptIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
            .addComponent(promptMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout promptLayout = new javax.swing.GroupLayout(prompt);
        prompt.setLayout(promptLayout);
        promptLayout.setHorizontalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        promptLayout.setVerticalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setMinimumSize(new java.awt.Dimension(1200, 800));
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.BorderLayout());

        Adverticement.setBackground(new java.awt.Color(51, 51, 51));
        Adverticement.setMaximumSize(new java.awt.Dimension(840, 700));
        Adverticement.setPreferredSize(new java.awt.Dimension(850, 793));

        javax.swing.GroupLayout AdverticementLayout = new javax.swing.GroupLayout(Adverticement);
        Adverticement.setLayout(AdverticementLayout);
        AdverticementLayout.setHorizontalGroup(
            AdverticementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 850, Short.MAX_VALUE)
        );
        AdverticementLayout.setVerticalGroup(
            AdverticementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 793, Short.MAX_VALUE)
        );

        jPanel1.add(Adverticement, java.awt.BorderLayout.LINE_START);

        main.setLayout(new java.awt.CardLayout());

        login.setBackground(new java.awt.Color(255, 255, 255));
        login.setLayout(null);

        loginLabel.setFont(new java.awt.Font("Roboto Light", 0, 36)); // NOI18N
        loginLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginLabel.setText("LOGIN");
        login.add(loginLabel);
        loginLabel.setBounds(10, 30, 520, 48);

        usernameLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        usernameLabel.setText("Employee ID:");
        login.add(usernameLabel);
        usernameLabel.setBounds(110, 170, 100, 31);

        userName.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        userName.setBorder(null);
        userName.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        userName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                userNameMouseClicked(evt);
            }
        });
        userName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userNameActionPerformed(evt);
            }
        });
        login.add(userName);
        userName.setBounds(110, 210, 320, 40);
        login.add(jSeparator4);
        jSeparator4.setBounds(110, 250, 320, 10);

        passwordLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        passwordLabel.setText("Password:");
        login.add(passwordLabel);
        passwordLabel.setBounds(110, 280, 86, 31);

        userPassword.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        userPassword.setBorder(null);
        userPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                userPasswordMouseClicked(evt);
            }
        });
        userPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                userPasswordKeyReleased(evt);
            }
        });
        login.add(userPassword);
        userPassword.setBounds(110, 320, 320, 40);
        login.add(jSeparator5);
        jSeparator5.setBounds(110, 360, 320, 10);

        LoginBtn.setBackground(new java.awt.Color(51, 51, 51));
        LoginBtn.setName(""); // NOI18N
        LoginBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LoginBtnMouseClicked(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(204, 204, 204));
        jLabel9.setText("EMAIL");

        jLabel12.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("LOGIN");

        javax.swing.GroupLayout LoginBtnLayout = new javax.swing.GroupLayout(LoginBtn);
        LoginBtn.setLayout(LoginBtnLayout);
        LoginBtnLayout.setHorizontalGroup(
            LoginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoginBtnLayout.createSequentialGroup()
                .addGap(500, 500, 500)
                .addComponent(jLabel9))
            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        LoginBtnLayout.setVerticalGroup(
            LoginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoginBtnLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel12)
                .addGap(301, 301, 301)
                .addComponent(jLabel9))
        );

        login.add(LoginBtn);
        LoginBtn.setBounds(200, 430, 150, 40);

        jLabel5.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("or");
        login.add(jLabel5);
        jLabel5.setBounds(260, 480, 21, 26);

        message.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        message.setForeground(new java.awt.Color(255, 51, 51));
        login.add(message);
        message.setBounds(110, 100, 320, 40);

        jLabel11.setFont(new java.awt.Font("Roboto Light", 0, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Create Account");
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
        });
        login.add(jLabel11);
        jLabel11.setBounds(220, 510, 110, 17);

        main.add(login, "card2");

        RegistrationPanel.setBackground(new java.awt.Color(255, 255, 255));
        RegistrationPanel.setLayout(null);

        RegLabel.setFont(new java.awt.Font("Roboto Light", 0, 36)); // NOI18N
        RegLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RegLabel.setText("Register");
        RegistrationPanel.add(RegLabel);
        RegLabel.setBounds(20, 60, 490, 48);

        RegIDLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        RegIDLabel.setText("Employee ID:");
        RegistrationPanel.add(RegIDLabel);
        RegIDLabel.setBounds(100, 200, 150, 19);

        regID.setFont(new java.awt.Font("Roboto Light", 0, 12)); // NOI18N
        regID.setBorder(null);
        regID.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        regID.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                regIDMouseClicked(evt);
            }
        });
        regID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regIDActionPerformed(evt);
            }
        });
        regID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                regIDKeyReleased(evt);
            }
        });
        RegistrationPanel.add(regID);
        regID.setBounds(100, 230, 320, 40);
        RegistrationPanel.add(jSeparator7);
        jSeparator7.setBounds(100, 270, 330, 10);

        passLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        passLabel.setText("Password:");
        RegistrationPanel.add(passLabel);
        passLabel.setBounds(100, 290, 140, 19);

        userPass.setBorder(null);
        userPass.setMaximumSize(new java.awt.Dimension(320, 30));
        userPass.setMinimumSize(new java.awt.Dimension(320, 30));
        userPass.setPreferredSize(new java.awt.Dimension(320, 30));
        userPass.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                userPassFocusGained(evt);
            }
        });
        userPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                userPassKeyReleased(evt);
            }
        });
        RegistrationPanel.add(userPass);
        userPass.setBounds(100, 320, 320, 40);
        RegistrationPanel.add(jSeparator9);
        jSeparator9.setBounds(100, 360, 330, 10);

        reEnterPassLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        reEnterPassLabel.setText("Re-enter Password:");
        RegistrationPanel.add(reEnterPassLabel);
        reEnterPassLabel.setBounds(100, 380, 210, 19);

        reEnterPass.setBorder(null);
        reEnterPass.setMaximumSize(new java.awt.Dimension(320, 30));
        reEnterPass.setMinimumSize(new java.awt.Dimension(320, 30));
        reEnterPass.setPreferredSize(new java.awt.Dimension(320, 30));
        reEnterPass.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                reEnterPassFocusGained(evt);
            }
        });
        reEnterPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                reEnterPassKeyReleased(evt);
            }
        });
        RegistrationPanel.add(reEnterPass);
        reEnterPass.setBounds(100, 410, 320, 40);
        RegistrationPanel.add(jSeparator6);
        jSeparator6.setBounds(100, 450, 330, 20);

        RegBtn.setBackground(new java.awt.Color(51, 51, 51));
        RegBtn.setName(""); // NOI18N
        RegBtn.setPreferredSize(new java.awt.Dimension(150, 40));

        jLabel20.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Register");
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout RegBtnLayout = new javax.swing.GroupLayout(RegBtn);
        RegBtn.setLayout(RegBtnLayout);
        RegBtnLayout.setHorizontalGroup(
            RegBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RegBtnLayout.createSequentialGroup()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(391, 391, 391))
        );
        RegBtnLayout.setVerticalGroup(
            RegBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RegBtnLayout.createSequentialGroup()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(310, 310, 310))
        );

        RegistrationPanel.add(RegBtn);
        RegBtn.setBounds(190, 510, 150, 40);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        RegistrationPanel.add(jLabel21);
        jLabel21.setBounds(30, 20, 50, 40);

        message1.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        message1.setForeground(new java.awt.Color(255, 51, 51));
        RegistrationPanel.add(message1);
        message1.setBounds(100, 120, 400, 50);

        main.add(RegistrationPanel, "card2");

        jPanel1.add(main, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(25, 25, 25));
        jPanel2.setMinimumSize(new java.awt.Dimension(100, 20));
        jPanel2.setPreferredSize(new java.awt.Dimension(1200, 20));

        jLabel1.setFont(new java.awt.Font("Roboto Light", 1, 10)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("X");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1372, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void userNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_userNameMouseClicked
        userName.setEditable(true);
        userName.setText("");
    }//GEN-LAST:event_userNameMouseClicked

    private void userNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNameActionPerformed

    }//GEN-LAST:event_userNameActionPerformed

    private void userPasswordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_userPasswordMouseClicked
        userPassword.setEditable(true);
        userPassword.setText("");
    }//GEN-LAST:event_userPasswordMouseClicked

    private void userPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_userPasswordKeyReleased
        if(evt.getKeyCode() == 10){
            login();
        }
    }//GEN-LAST:event_userPasswordKeyReleased

    private void LoginBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LoginBtnMouseClicked
        login();
    }//GEN-LAST:event_LoginBtnMouseClicked

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
        switchToPanel(main, RegistrationPanel);
    }//GEN-LAST:event_jLabel11MouseClicked

    private void regIDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_regIDMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_regIDMouseClicked

    private void regIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_regIDActionPerformed

    private void regIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_regIDKeyReleased
        if(evt.getKeyCode() == 10){
            reg();
        }
    }//GEN-LAST:event_regIDKeyReleased

    private void userPassFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_userPassFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_userPassFocusGained

    private void userPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_userPassKeyReleased
        if(evt.getKeyCode() == 10){
            reg();
        }
    }//GEN-LAST:event_userPassKeyReleased

    private void reEnterPassFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_reEnterPassFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_reEnterPassFocusGained

    private void reEnterPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_reEnterPassKeyReleased
        if(evt.getKeyCode() == 10){
            reg();
        }
    }//GEN-LAST:event_reEnterPassKeyReleased

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
        reg();
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        main.removeAll();
        main.add(login);
        main.revalidate();
        main.repaint();
    }//GEN-LAST:event_jLabel21MouseClicked
        
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Authentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Authentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Authentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Authentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Authentication().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Adverticement;
    private javax.swing.JPanel LoginBtn;
    private javax.swing.JPanel RegBtn;
    private javax.swing.JLabel RegIDLabel;
    private javax.swing.JLabel RegLabel;
    private javax.swing.JPanel RegistrationPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JPanel login;
    private javax.swing.JLabel loginLabel;
    private javax.swing.JPanel main;
    private javax.swing.JLabel message;
    private javax.swing.JLabel message1;
    private javax.swing.JLabel passLabel;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JPanel prompt;
    private javax.swing.JLabel promptIcon;
    private javax.swing.JLabel promptMsg;
    private javax.swing.JPasswordField reEnterPass;
    private javax.swing.JLabel reEnterPassLabel;
    private javax.swing.JTextField regID;
    private javax.swing.JTextField userName;
    private javax.swing.JPasswordField userPass;
    private javax.swing.JPasswordField userPassword;
    private javax.swing.JLabel usernameLabel;
    // End of variables declaration//GEN-END:variables

    public void switchToPanel(JPanel main, JPanel panel){
        main.removeAll();
        main.add(panel);
        main.revalidate();
        main.repaint();
    }
    
    /**
     * 
     */
    public void saved(){
        regID.setText("");
        userPass.setText("");
        reEnterPass.setText("");
        message1.setText("");
        message.setText("");
    }
    
    public void log(){
        userName.setText("");
        userPassword.setText("");
        message.setText("");
    }
    
    public void loginPanel(){
        main.removeAll();
        main.add(login);
        main.revalidate();
        main.repaint();
    }

    private void login() {
        String empID = userName.getText();
        String userPass = String.valueOf(userPassword.getText());
        
        if(!empID.isEmpty() && !userPass.isEmpty()){
            int result = auth.checkLoginId(empID);
            if(result == 1){
                if (passwordEncryptor.checkPassword(userPass, auth.getPassword(empID))) {
                    switch(auth.getJobTitle(empID)){
                        case 1:
                            customerService.setName(auth.getEmployeeName(empID), empID);
                            customerService.setVisible(true);
                            setVisible(false);
                            break;
                        case 2:
                            inventory.setName(auth.getEmployeeName(empID), empID);
                            inventory.setVisible(true);
                            setVisible(false);
                            break;
                        case 3:
                            break;
                        default:
                            admin.setVisible(true);
                            setVisible(false);
                            break;
                    }
                    log();
                }
                else{
                    popUp.showMessage("ERROR: Password is incorrect!", 1);
                }
            }
            else{
            	popUp.showMessage("ERROR: Id is invalid!", 1);
            }
        }
        else{
            popUp.showMessage("ERROR: Some fields are empty!", 1);
        }
    }

    private void reg() {
        String id = regID.getText();
        String pass = userPass.getText();
        String pass1 = reEnterPass.getText();
        
        int result = auth.checkRegisterId(id);
        if (!id.isEmpty() && !pass.isEmpty() && !pass1.isEmpty()) {
            if (result == 1) {
                if (pass.equals(pass1)) {
                    int check = auth.addAccount(id, passwordEncryptor.encryptPassword(pass));
                    if (check == 0) {
                        switchToPanel(main, login);
                        popUp.showMessage("Account has been registered successfully!", 0);
                        saved();
                    } else {
                        popUp.showMessage("ERROR: Account was already registered!", 1);
                    }
                } else {
                        popUp.showMessage("ERROR: Password mismatched!", 1);
                }
            } else {
                popUp.showMessage("ERROR: Id is invalid!", 1);
            }
        }
        else{
            popUp.showMessage("ERROR: Some fields are empty!", 1);
        }
    }
    
    
}
