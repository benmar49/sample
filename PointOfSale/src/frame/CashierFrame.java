package frame;

import control.Authenticate;
import control.Controller;
import control.PopUpMessage;
import customtable.CustomTableModel;
import employee.Cashier;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import customtable.CustomTableHeader;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import net.sf.jasperreports.swing.JRViewer;
import org.jasypt.util.password.StrongPasswordEncryptor;
import reports.Reports;

public class CashierFrame extends javax.swing.JFrame {
    
    StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    Controller controller = new Controller();
    Cashier cashier = new Cashier();
    private Authenticate auth = new Authenticate();
    private PopUpMessage popUp;
    private boolean save = false;
    private boolean success = false;
    private boolean pending = false;
    private int iterate = 0;
    private double tot = 0;
    private double subTotal = 0;
    private double subTotal1 = 0;
    private String lastName,midName,firstName,custId;
    private Reports reports = new Reports();
    private boolean authenticate = true;
    private boolean couponStatus = false;
    private int row;
    private int col;
    JRViewer view;

    private ArrayList<String> data = new ArrayList<>();
    
    public CashierFrame() {
        initComponents();
        setLocationRelativeTo(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        initialize();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        payFrame = new javax.swing.JDialog();
        jPanel34 = new javax.swing.JPanel();
        jPanel35 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        printBtn1 = new javax.swing.JLabel();
        main1 = new javax.swing.JPanel();
        amt1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel38 = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        inputAmount1 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        total2 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        change1 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        changeValue1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cusNamePayLabel = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        pointsEarnedLabel = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        msg1 = new javax.swing.JLabel();
        prompt = new javax.swing.JPanel();
        jPanel28 = new javax.swing.JPanel();
        promptIcon = new javax.swing.JLabel();
        promptMsg = new javax.swing.JLabel();
        pointIdFrame = new javax.swing.JDialog();
        jPanel31 = new javax.swing.JPanel();
        jPanel32 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        inputCustId = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        login = new javax.swing.JDialog();
        jPanel59 = new javax.swing.JPanel();
        jPanel60 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        loginMain = new javax.swing.JPanel();
        loginPanel = new javax.swing.JPanel();
        loginId = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel31 = new javax.swing.JLabel();
        loginBtn = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        loginPass = new javax.swing.JPasswordField();
        createAccountBtn = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        registerPanel = new javax.swing.JPanel();
        registerId = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel35 = new javax.swing.JLabel();
        registerBtn = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        registerPassword = new javax.swing.JPasswordField();
        jLabel37 = new javax.swing.JLabel();
        registerPassword1 = new javax.swing.JPasswordField();
        jSeparator7 = new javax.swing.JSeparator();
        loginBack = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        updateQty = new javax.swing.JDialog();
        jPanel9 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel71 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        add = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        minus = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        updateQuantity = new javax.swing.JPanel();
        jLabel70 = new javax.swing.JLabel();
        supervisorLoginFrame = new javax.swing.JDialog();
        jPanel65 = new javax.swing.JPanel();
        jPanel66 = new javax.swing.JPanel();
        jLabel72 = new javax.swing.JLabel();
        jLabel83 = new javax.swing.JLabel();
        loginMain1 = new javax.swing.JPanel();
        registerPanel1 = new javax.swing.JPanel();
        loginBtn1 = new javax.swing.JPanel();
        jLabel80 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        pinPass = new javax.swing.JPasswordField();
        jSeparator14 = new javax.swing.JSeparator();
        list = new javax.swing.JDialog();
        jPanel71 = new javax.swing.JPanel();
        jPanel72 = new javax.swing.JPanel();
        close = new javax.swing.JLabel();
        jLabel84 = new javax.swing.JLabel();
        loginMain2 = new javax.swing.JPanel();
        registerPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        qItem = new javax.swing.JTable();
        jPanel24 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        cancelBtn = new javax.swing.JPanel();
        jLabel78 = new javax.swing.JLabel();
        couponFrame = new javax.swing.JDialog();
        jPanel73 = new javax.swing.JPanel();
        jPanel74 = new javax.swing.JPanel();
        jLabel77 = new javax.swing.JLabel();
        jPanel75 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        inputCoupCode = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        supervisorLoginFrameQuantity = new javax.swing.JDialog();
        jPanel76 = new javax.swing.JPanel();
        jPanel77 = new javax.swing.JPanel();
        closeBtnQuan = new javax.swing.JLabel();
        jLabel85 = new javax.swing.JLabel();
        loginMain3 = new javax.swing.JPanel();
        closeBtn = new javax.swing.JPanel();
        loginBtnQuan = new javax.swing.JPanel();
        jLabel86 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        pinPassQuan = new javax.swing.JPasswordField();
        jSeparator15 = new javax.swing.JSeparator();
        panel = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        cashierId = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        transNo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        invoiceId = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        customerId = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        customerName = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        customerPoints = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        barcode1 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        prodName1 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        desc1 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        qty1 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jPanel39 = new javax.swing.JPanel();
        jPanel40 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jPanel41 = new javax.swing.JPanel();
        jPanel42 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jPanel43 = new javax.swing.JPanel();
        jPanel44 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jPanel45 = new javax.swing.JPanel();
        jPanel46 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jPanel47 = new javax.swing.JPanel();
        jPanel48 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jPanel49 = new javax.swing.JPanel();
        jPanel50 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jPanel51 = new javax.swing.JPanel();
        jPanel52 = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jPanel53 = new javax.swing.JPanel();
        jPanel54 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jPanel55 = new javax.swing.JPanel();
        jPanel56 = new javax.swing.JPanel();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jPanel57 = new javax.swing.JPanel();
        jPanel58 = new javax.swing.JPanel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jPanel61 = new javax.swing.JPanel();
        jPanel62 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jPanel63 = new javax.swing.JPanel();
        jPanel64 = new javax.swing.JPanel();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        mainTransactionPanel = new javax.swing.JPanel();
        transactionPanel = new javax.swing.JPanel();
        jPanel29 = new javax.swing.JPanel();
        quantity = new javax.swing.JTextField();
        searchProduct = new javax.swing.JTextField();
        jPanel21 = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        mainPanel = new javax.swing.JPanel();
        panel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        transTable = new org.jdesktop.swingx.JXTable();
        panel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        productTable = new org.jdesktop.swingx.JXTable();
        pendingPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        pendingTable = new org.jdesktop.swingx.JXTable();
        text = new javax.swing.JLabel();
        jPanel = new javax.swing.JPanel();

        payFrame.setMinimumSize(new java.awt.Dimension(695, 543));
        payFrame.setModal(true);
        payFrame.setUndecorated(true);

        jPanel34.setLayout(new java.awt.BorderLayout());

        jPanel35.setBackground(new java.awt.Color(25, 51, 76));
        jPanel35.setPreferredSize(new java.awt.Dimension(605, 60));

        jLabel9.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("X");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel35Layout.createSequentialGroup()
                .addGap(0, 645, Short.MAX_VALUE)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel34.add(jPanel35, java.awt.BorderLayout.PAGE_START);

        jPanel36.setBackground(new java.awt.Color(255, 255, 255));
        jPanel36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel37.setBackground(new java.awt.Color(25, 51, 76));

        printBtn1.setFont(new java.awt.Font("Roboto Light", 0, 48)); // NOI18N
        printBtn1.setForeground(new java.awt.Color(255, 255, 255));
        printBtn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        printBtn1.setText("Print Receipt");
        printBtn1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                printBtn1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(printBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(printBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        main1.setLayout(new java.awt.CardLayout());

        amt1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel11.setFont(new java.awt.Font("Roboto Light", 0, 70)); // NOI18N
        jLabel11.setText("P");

        jPanel38.setOpaque(false);
        jPanel38.setLayout(null);
        jPanel38.add(jSeparator2);
        jSeparator2.setBounds(0, 90, 350, 20);

        inputAmount1.setFont(new java.awt.Font("Roboto Light", 0, 70)); // NOI18N
        inputAmount1.setBorder(null);
        inputAmount1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                inputAmount1KeyReleased(evt);
            }
        });
        jPanel38.add(inputAmount1);
        inputAmount1.setBounds(0, 0, 360, 93);

        jLabel21.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        jLabel21.setText("AMOUNT:");

        total2.setFont(new java.awt.Font("Roboto Light", 0, 70)); // NOI18N
        total2.setText("P900,000.00");
        total2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel40.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        jLabel40.setText("TOTAL :");

        javax.swing.GroupLayout amt1Layout = new javax.swing.GroupLayout(amt1);
        amt1.setLayout(amt1Layout);
        amt1Layout.setHorizontalGroup(
            amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(amt1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(amt1Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(total2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        amt1Layout.setVerticalGroup(
            amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(amt1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(total2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(amt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addContainerGap())
        );

        main1.add(amt1, "card2");

        change1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel41.setFont(new java.awt.Font("Roboto Light", 0, 50)); // NOI18N
        jLabel41.setText("Change:");

        changeValue1.setFont(new java.awt.Font("Roboto Light", 0, 50)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel2.setText("Congratulations!");

        cusNamePayLabel.setFont(new java.awt.Font("Roboto Light", 1, 36)); // NOI18N
        cusNamePayLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cusNamePayLabel.setText("Joseph John");

        jLabel17.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel17.setText("You have earned ");

        pointsEarnedLabel.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        pointsEarnedLabel.setText("0723");

        jLabel42.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel42.setText("Point(s)");

        javax.swing.GroupLayout change1Layout = new javax.swing.GroupLayout(change1);
        change1.setLayout(change1Layout);
        change1Layout.setHorizontalGroup(
            change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(change1Layout.createSequentialGroup()
                .addGroup(change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(change1Layout.createSequentialGroup()
                        .addGap(243, 243, 243)
                        .addComponent(jLabel2))
                    .addGroup(change1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(change1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(changeValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(change1Layout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addComponent(pointsEarnedLabel)
                                .addGap(55, 55, 55)
                                .addComponent(jLabel42)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, change1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(cusNamePayLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78))
        );
        change1Layout.setVerticalGroup(
            change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, change1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cusNamePayLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(pointsEarnedLabel)
                    .addComponent(jLabel42))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(change1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel41)
                    .addComponent(changeValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        main1.add(change1, "card2");

        msg1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        msg1.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                        .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(156, 156, 156))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                        .addComponent(main1, javax.swing.GroupLayout.PREFERRED_SIZE, 673, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                        .addComponent(msg1, javax.swing.GroupLayout.PREFERRED_SIZE, 673, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addComponent(msg1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(main1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel34.add(jPanel36, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout payFrameLayout = new javax.swing.GroupLayout(payFrame.getContentPane());
        payFrame.getContentPane().setLayout(payFrameLayout);
        payFrameLayout.setHorizontalGroup(
            payFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        payFrameLayout.setVerticalGroup(
            payFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
        );

        prompt.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        jPanel28.setBackground(new java.awt.Color(255, 255, 255));
        jPanel28.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        promptIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/check.png"))); // NOI18N

        promptMsg.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        promptMsg.setForeground(new java.awt.Color(0, 153, 51));
        promptMsg.setText("PROMPT_MESSAGE");

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(promptIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(promptMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(promptIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
            .addComponent(promptMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout promptLayout = new javax.swing.GroupLayout(prompt);
        prompt.setLayout(promptLayout);
        promptLayout.setHorizontalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        promptLayout.setVerticalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pointIdFrame.setMinimumSize(new java.awt.Dimension(457, 272));
        pointIdFrame.setUndecorated(true);

        jPanel31.setBackground(new java.awt.Color(255, 255, 255));
        jPanel31.setMaximumSize(new java.awt.Dimension(457, 272));
        jPanel31.setMinimumSize(new java.awt.Dimension(457, 272));
        jPanel31.setName(""); // NOI18N
        jPanel31.setPreferredSize(new java.awt.Dimension(366, 164));

        jPanel32.setBackground(new java.awt.Color(102, 255, 102));
        jPanel32.setForeground(new java.awt.Color(255, 255, 255));
        jPanel32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel32MouseClicked(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel43.setForeground(new java.awt.Color(255, 255, 255));
        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("CONFIRM");

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel33.setBackground(new java.awt.Color(255, 51, 51));
        jPanel33.setForeground(new java.awt.Color(255, 255, 255));
        jPanel33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel33MouseClicked(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel44.setForeground(new java.awt.Color(255, 255, 255));
        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("CANCEL");

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel45.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel45.setText("Input Customer ID");

        inputCustId.setFont(new java.awt.Font("Roboto Light", 0, 70)); // NOI18N
        inputCustId.setBorder(null);
        inputCustId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                inputCustIdKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(inputCustId, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel31Layout.createSequentialGroup()
                            .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE))
                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58))
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel45)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inputCustId, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pointIdFrameLayout = new javax.swing.GroupLayout(pointIdFrame.getContentPane());
        pointIdFrame.getContentPane().setLayout(pointIdFrameLayout);
        pointIdFrameLayout.setHorizontalGroup(
            pointIdFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        pointIdFrameLayout.setVerticalGroup(
            pointIdFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        login.setMinimumSize(new java.awt.Dimension(695, 543));
        login.setModal(true);
        login.setModalExclusionType(null);
        login.setUndecorated(true);
        login.setType(java.awt.Window.Type.POPUP);

        jPanel59.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51)));
        jPanel59.setMinimumSize(new java.awt.Dimension(699, 543));
        jPanel59.setLayout(new java.awt.BorderLayout());

        jPanel60.setBackground(new java.awt.Color(25, 51, 76));
        jPanel60.setPreferredSize(new java.awt.Dimension(605, 60));

        jLabel29.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("X");
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel29MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel60Layout = new javax.swing.GroupLayout(jPanel60);
        jPanel60.setLayout(jPanel60Layout);
        jPanel60Layout.setHorizontalGroup(
            jPanel60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel60Layout.createSequentialGroup()
                .addContainerGap(643, Short.MAX_VALUE)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel60Layout.setVerticalGroup(
            jPanel60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel59.add(jPanel60, java.awt.BorderLayout.PAGE_START);

        loginMain.setBackground(new java.awt.Color(255, 255, 255));
        loginMain.setLayout(new java.awt.CardLayout());

        loginPanel.setBackground(new java.awt.Color(255, 255, 255));
        loginPanel.setLayout(null);

        loginId.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        loginId.setBorder(null);
        loginId.setMargin(new java.awt.Insets(2, 20, 2, 2));
        loginId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                loginIdKeyReleased(evt);
            }
        });
        loginPanel.add(loginId);
        loginId.setBounds(140, 130, 430, 50);

        jLabel30.setFont(new java.awt.Font("Roboto Light", 1, 36)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("Login");
        loginPanel.add(jLabel30);
        jLabel30.setBounds(140, 20, 430, 50);
        loginPanel.add(jSeparator1);
        jSeparator1.setBounds(140, 180, 430, 10);
        loginPanel.add(jSeparator4);
        jSeparator4.setBounds(140, 280, 430, 20);

        jLabel31.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel31.setText("Password:");
        loginPanel.add(jLabel31);
        jLabel31.setBounds(140, 200, 220, 24);

        loginBtn.setBackground(new java.awt.Color(25, 51, 76));
        loginBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnMouseClicked(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("Login");

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        loginPanel.add(loginBtn);
        loginBtn.setBounds(280, 320, 160, 50);

        loginPass.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        loginPass.setBorder(null);
        loginPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                loginPassKeyReleased(evt);
            }
        });
        loginPanel.add(loginPass);
        loginPass.setBounds(140, 230, 430, 50);

        createAccountBtn.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        createAccountBtn.setForeground(new java.awt.Color(0, 51, 102));
        createAccountBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        createAccountBtn.setText("Create Account");
        createAccountBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        createAccountBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                createAccountBtnMouseClicked(evt);
            }
        });
        loginPanel.add(createAccountBtn);
        createAccountBtn.setBounds(280, 430, 160, 24);

        jLabel15.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("or");
        loginPanel.add(jLabel15);
        jLabel15.setBounds(280, 390, 160, 24);

        jLabel66.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel66.setText("Cashier ID:");
        loginPanel.add(jLabel66);
        jLabel66.setBounds(140, 100, 220, 24);

        loginMain.add(loginPanel, "card2");

        registerPanel.setBackground(new java.awt.Color(255, 255, 255));
        registerPanel.setLayout(null);

        registerId.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        registerId.setBorder(null);
        registerId.setMargin(new java.awt.Insets(2, 20, 2, 2));
        registerId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                registerIdKeyReleased(evt);
            }
        });
        registerPanel.add(registerId);
        registerId.setBounds(150, 110, 430, 50);

        jLabel34.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel34.setText("Cashier ID:");
        registerPanel.add(jLabel34);
        jLabel34.setBounds(150, 80, 220, 24);
        registerPanel.add(jSeparator5);
        jSeparator5.setBounds(150, 160, 430, 10);
        registerPanel.add(jSeparator6);
        jSeparator6.setBounds(150, 250, 430, 20);

        jLabel35.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel35.setText("Password:");
        registerPanel.add(jLabel35);
        jLabel35.setBounds(150, 170, 220, 24);

        registerBtn.setBackground(new java.awt.Color(25, 51, 76));
        registerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                registerBtnMouseClicked(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("Register");

        javax.swing.GroupLayout registerBtnLayout = new javax.swing.GroupLayout(registerBtn);
        registerBtn.setLayout(registerBtnLayout);
        registerBtnLayout.setHorizontalGroup(
            registerBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        registerBtnLayout.setVerticalGroup(
            registerBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        registerPanel.add(registerBtn);
        registerBtn.setBounds(280, 400, 160, 50);

        registerPassword.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        registerPassword.setBorder(null);
        registerPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                registerPasswordKeyReleased(evt);
            }
        });
        registerPanel.add(registerPassword);
        registerPassword.setBounds(150, 200, 430, 50);

        jLabel37.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel37.setText("Re-enter Password:");
        registerPanel.add(jLabel37);
        jLabel37.setBounds(150, 260, 220, 24);

        registerPassword1.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        registerPassword1.setBorder(null);
        registerPassword1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                registerPassword1KeyReleased(evt);
            }
        });
        registerPanel.add(registerPassword1);
        registerPassword1.setBounds(150, 290, 430, 50);
        registerPanel.add(jSeparator7);
        jSeparator7.setBounds(150, 340, 430, 20);

        loginBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        loginBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBackMouseClicked(evt);
            }
        });
        registerPanel.add(loginBack);
        loginBack.setBounds(30, 20, 50, 50);

        jLabel67.setFont(new java.awt.Font("Roboto Light", 1, 36)); // NOI18N
        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel67.setText("Register");
        registerPanel.add(jLabel67);
        jLabel67.setBounds(150, 10, 430, 50);

        loginMain.add(registerPanel, "card2");

        jPanel59.add(loginMain, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout loginLayout = new javax.swing.GroupLayout(login.getContentPane());
        login.getContentPane().setLayout(loginLayout);
        loginLayout.setHorizontalGroup(
            loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel59, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)
        );
        loginLayout.setVerticalGroup(
            loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        updateQty.setMinimumSize(new java.awt.Dimension(468, 365));
        updateQty.setModal(true);
        updateQty.setUndecorated(true);

        jPanel9.setMinimumSize(new java.awt.Dimension(468, 372));
        jPanel9.setName(""); // NOI18N
        jPanel9.setLayout(new java.awt.BorderLayout());

        jPanel16.setBackground(new java.awt.Color(25, 51, 76));
        jPanel16.setPreferredSize(new java.awt.Dimension(468, 40));

        jLabel71.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel71.setForeground(new java.awt.Color(255, 255, 255));
        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel71.setText("X");
        jLabel71.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel71MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addGap(0, 428, Short.MAX_VALUE)
                .addComponent(jLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel71, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel9.add(jPanel16, java.awt.BorderLayout.PAGE_START);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setLayout(null);

        add.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        add.setBorder(null);
        add.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                addFocusLost(evt);
            }
        });
        add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                addKeyReleased(evt);
            }
        });
        jPanel17.add(add);
        add.setBounds(160, 50, 260, 60);

        jPanel19.setBackground(new java.awt.Color(255, 51, 51));

        jLabel38.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("-");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        jPanel17.add(jPanel19);
        jPanel19.setBounds(60, 120, 80, 70);

        jPanel20.setBackground(new java.awt.Color(0, 204, 51));

        jLabel13.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("+");

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        jPanel17.add(jPanel20);
        jPanel20.setBounds(60, 40, 80, 70);
        jPanel17.add(jSeparator8);
        jSeparator8.setBounds(160, 110, 260, 10);

        minus.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        minus.setBorder(null);
        minus.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                minusFocusLost(evt);
            }
        });
        minus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                minusKeyReleased(evt);
            }
        });
        jPanel17.add(minus);
        minus.setBounds(160, 130, 260, 60);
        jPanel17.add(jSeparator9);
        jSeparator9.setBounds(160, 190, 260, 10);

        updateQuantity.setBackground(new java.awt.Color(25, 51, 76));
        updateQuantity.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateQuantityMouseClicked(evt);
            }
        });

        jLabel70.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        jLabel70.setForeground(new java.awt.Color(255, 255, 255));
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel70.setText("Update Quantity");

        javax.swing.GroupLayout updateQuantityLayout = new javax.swing.GroupLayout(updateQuantity);
        updateQuantity.setLayout(updateQuantityLayout);
        updateQuantityLayout.setHorizontalGroup(
            updateQuantityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel70, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
        );
        updateQuantityLayout.setVerticalGroup(
            updateQuantityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel70, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel17.add(updateQuantity);
        updateQuantity.setBounds(120, 230, 240, 60);

        jPanel9.add(jPanel17, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout updateQtyLayout = new javax.swing.GroupLayout(updateQty.getContentPane());
        updateQty.getContentPane().setLayout(updateQtyLayout);
        updateQtyLayout.setHorizontalGroup(
            updateQtyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        updateQtyLayout.setVerticalGroup(
            updateQtyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 365, Short.MAX_VALUE)
        );

        supervisorLoginFrame.setMinimumSize(new java.awt.Dimension(607, 410));
        supervisorLoginFrame.setModal(true);
        supervisorLoginFrame.setModalExclusionType(null);
        supervisorLoginFrame.setUndecorated(true);
        supervisorLoginFrame.setType(java.awt.Window.Type.POPUP);

        jPanel65.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51)));
        jPanel65.setMinimumSize(new java.awt.Dimension(699, 543));
        jPanel65.setLayout(new java.awt.BorderLayout());

        jPanel66.setBackground(new java.awt.Color(25, 51, 76));
        jPanel66.setPreferredSize(new java.awt.Dimension(605, 60));

        jLabel72.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        jLabel72.setForeground(new java.awt.Color(255, 255, 255));
        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel72.setText("X");
        jLabel72.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel72MouseClicked(evt);
            }
        });

        jLabel83.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel83.setForeground(new java.awt.Color(255, 255, 255));
        jLabel83.setText("    Supervisor's Approval");
        jLabel83.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel83MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel66Layout = new javax.swing.GroupLayout(jPanel66);
        jPanel66.setLayout(jPanel66Layout);
        jPanel66Layout.setHorizontalGroup(
            jPanel66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel66Layout.createSequentialGroup()
                .addComponent(jLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, 533, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel72, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
        );
        jPanel66Layout.setVerticalGroup(
            jPanel66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel72, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
            .addComponent(jLabel83, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel65.add(jPanel66, java.awt.BorderLayout.PAGE_START);

        loginMain1.setBackground(new java.awt.Color(255, 255, 255));
        loginMain1.setLayout(new java.awt.CardLayout());

        registerPanel1.setBackground(new java.awt.Color(255, 255, 255));
        registerPanel1.setLayout(null);

        loginBtn1.setBackground(new java.awt.Color(25, 51, 76));
        loginBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtn1MouseClicked(evt);
            }
        });
        loginBtn1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                loginBtn1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                loginBtn1KeyReleased(evt);
            }
        });

        jLabel80.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel80.setForeground(new java.awt.Color(255, 255, 255));
        jLabel80.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel80.setText("Approved");

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel80, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel80, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        registerPanel1.add(loginBtn1);
        loginBtn1.setBounds(220, 210, 160, 50);

        jLabel81.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel81.setText("Input Supervisor Pin #:");
        registerPanel1.add(jLabel81);
        jLabel81.setBounds(90, 70, 220, 24);

        pinPass.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        pinPass.setBorder(null);
        pinPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pinPassKeyReleased(evt);
            }
        });
        registerPanel1.add(pinPass);
        pinPass.setBounds(90, 100, 430, 50);
        registerPanel1.add(jSeparator14);
        jSeparator14.setBounds(90, 150, 430, 20);

        loginMain1.add(registerPanel1, "card2");

        jPanel65.add(loginMain1, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout supervisorLoginFrameLayout = new javax.swing.GroupLayout(supervisorLoginFrame.getContentPane());
        supervisorLoginFrame.getContentPane().setLayout(supervisorLoginFrameLayout);
        supervisorLoginFrameLayout.setHorizontalGroup(
            supervisorLoginFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        supervisorLoginFrameLayout.setVerticalGroup(
            supervisorLoginFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel65, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        list.setMinimumSize(new java.awt.Dimension(745, 608));
        list.setModal(true);
        list.setModalExclusionType(null);
        list.setUndecorated(true);
        list.setType(java.awt.Window.Type.POPUP);

        jPanel71.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51)));
        jPanel71.setMinimumSize(new java.awt.Dimension(699, 543));
        jPanel71.setLayout(new java.awt.BorderLayout());

        jPanel72.setBackground(new java.awt.Color(25, 51, 76));
        jPanel72.setPreferredSize(new java.awt.Dimension(605, 60));

        close.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        close.setForeground(new java.awt.Color(255, 255, 255));
        close.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        close.setText("X");
        close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeMouseClicked(evt);
            }
        });

        jLabel84.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel84.setForeground(new java.awt.Color(255, 255, 255));
        jLabel84.setText("    Selected Product");
        jLabel84.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel84MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel72Layout = new javax.swing.GroupLayout(jPanel72);
        jPanel72.setLayout(jPanel72Layout);
        jPanel72Layout.setHorizontalGroup(
            jPanel72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel72Layout.createSequentialGroup()
                .addComponent(jLabel84, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(close, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel72Layout.setVerticalGroup(
            jPanel72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(close, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
            .addComponent(jLabel84, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel71.add(jPanel72, java.awt.BorderLayout.PAGE_START);

        loginMain2.setBackground(new java.awt.Color(255, 255, 255));
        loginMain2.setLayout(new java.awt.CardLayout());

        registerPanel2.setBackground(new java.awt.Color(255, 255, 255));
        registerPanel2.setLayout(null);

        qItem.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        qItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(qItem);

        registerPanel2.add(jScrollPane4);
        jScrollPane4.setBounds(20, 20, 700, 450);

        jPanel24.setBackground(new java.awt.Color(25, 51, 76));

        jLabel39.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Delete");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        registerPanel2.add(jPanel24);
        jPanel24.setBounds(200, 480, 130, 50);

        cancelBtn.setBackground(new java.awt.Color(25, 51, 76));
        cancelBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelBtnMouseClicked(evt);
            }
        });

        jLabel78.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel78.setForeground(new java.awt.Color(255, 255, 255));
        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel78.setText("Cancel");

        javax.swing.GroupLayout cancelBtnLayout = new javax.swing.GroupLayout(cancelBtn);
        cancelBtn.setLayout(cancelBtnLayout);
        cancelBtnLayout.setHorizontalGroup(
            cancelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
        );
        cancelBtnLayout.setVerticalGroup(
            cancelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        registerPanel2.add(cancelBtn);
        cancelBtn.setBounds(410, 480, 130, 50);

        loginMain2.add(registerPanel2, "card2");

        jPanel71.add(loginMain2, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout listLayout = new javax.swing.GroupLayout(list.getContentPane());
        list.getContentPane().setLayout(listLayout);
        listLayout.setHorizontalGroup(
            listLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel71, javax.swing.GroupLayout.DEFAULT_SIZE, 745, Short.MAX_VALUE)
        );
        listLayout.setVerticalGroup(
            listLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel71, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
        );

        couponFrame.setMinimumSize(new java.awt.Dimension(457, 272));
        couponFrame.setUndecorated(true);

        jPanel73.setBackground(new java.awt.Color(255, 255, 255));
        jPanel73.setMaximumSize(new java.awt.Dimension(457, 272));
        jPanel73.setMinimumSize(new java.awt.Dimension(457, 272));
        jPanel73.setName(""); // NOI18N
        jPanel73.setPreferredSize(new java.awt.Dimension(366, 164));

        jPanel74.setBackground(new java.awt.Color(102, 255, 102));
        jPanel74.setForeground(new java.awt.Color(255, 255, 255));
        jPanel74.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel74MouseClicked(evt);
            }
        });

        jLabel77.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel77.setForeground(new java.awt.Color(255, 255, 255));
        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel77.setText("CONFIRM");

        javax.swing.GroupLayout jPanel74Layout = new javax.swing.GroupLayout(jPanel74);
        jPanel74.setLayout(jPanel74Layout);
        jPanel74Layout.setHorizontalGroup(
            jPanel74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel74Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel77, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel74Layout.setVerticalGroup(
            jPanel74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel74Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel77, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel75.setBackground(new java.awt.Color(255, 51, 51));
        jPanel75.setForeground(new java.awt.Color(255, 255, 255));
        jPanel75.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel75MouseClicked(evt);
            }
        });

        jLabel79.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel79.setForeground(new java.awt.Color(255, 255, 255));
        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel79.setText("CANCEL");

        javax.swing.GroupLayout jPanel75Layout = new javax.swing.GroupLayout(jPanel75);
        jPanel75.setLayout(jPanel75Layout);
        jPanel75Layout.setHorizontalGroup(
            jPanel75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel75Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel79, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel75Layout.setVerticalGroup(
            jPanel75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel75Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel79, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel82.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel82.setText("Input Coupon Code");

        inputCoupCode.setFont(new java.awt.Font("Roboto Light", 0, 70)); // NOI18N
        inputCoupCode.setBorder(null);
        inputCoupCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                inputCoupCodeKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel73Layout = new javax.swing.GroupLayout(jPanel73);
        jPanel73.setLayout(jPanel73Layout);
        jPanel73Layout.setHorizontalGroup(
            jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel73Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(inputCoupCode, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel73Layout.createSequentialGroup()
                            .addComponent(jPanel74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jSeparator10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE))
                    .addComponent(jLabel82, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58))
        );
        jPanel73Layout.setVerticalGroup(
            jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel73Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel82)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inputCoupCode, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout couponFrameLayout = new javax.swing.GroupLayout(couponFrame.getContentPane());
        couponFrame.getContentPane().setLayout(couponFrameLayout);
        couponFrameLayout.setHorizontalGroup(
            couponFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel73, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        couponFrameLayout.setVerticalGroup(
            couponFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel73, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        supervisorLoginFrameQuantity.setMinimumSize(new java.awt.Dimension(607, 410));
        supervisorLoginFrameQuantity.setModal(true);
        supervisorLoginFrameQuantity.setModalExclusionType(null);
        supervisorLoginFrameQuantity.setUndecorated(true);
        supervisorLoginFrameQuantity.setType(java.awt.Window.Type.POPUP);

        jPanel76.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51)));
        jPanel76.setMinimumSize(new java.awt.Dimension(699, 543));
        jPanel76.setLayout(new java.awt.BorderLayout());

        jPanel77.setBackground(new java.awt.Color(25, 51, 76));
        jPanel77.setPreferredSize(new java.awt.Dimension(605, 60));

        closeBtnQuan.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        closeBtnQuan.setForeground(new java.awt.Color(255, 255, 255));
        closeBtnQuan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        closeBtnQuan.setText("X");
        closeBtnQuan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeBtnQuanMouseClicked(evt);
            }
        });

        jLabel85.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        jLabel85.setForeground(new java.awt.Color(255, 255, 255));
        jLabel85.setText("    Supervisor's Approval");
        jLabel85.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel85MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel77Layout = new javax.swing.GroupLayout(jPanel77);
        jPanel77.setLayout(jPanel77Layout);
        jPanel77Layout.setHorizontalGroup(
            jPanel77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel77Layout.createSequentialGroup()
                .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, 533, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(closeBtnQuan, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
        );
        jPanel77Layout.setVerticalGroup(
            jPanel77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(closeBtnQuan, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
            .addComponent(jLabel85, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel76.add(jPanel77, java.awt.BorderLayout.PAGE_START);

        loginMain3.setBackground(new java.awt.Color(255, 255, 255));
        loginMain3.setLayout(new java.awt.CardLayout());

        closeBtn.setBackground(new java.awt.Color(255, 255, 255));
        closeBtn.setLayout(null);

        loginBtnQuan.setBackground(new java.awt.Color(25, 51, 76));
        loginBtnQuan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnQuanMouseClicked(evt);
            }
        });
        loginBtnQuan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                loginBtnQuanKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                loginBtnQuanKeyReleased(evt);
            }
        });

        jLabel86.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel86.setForeground(new java.awt.Color(255, 255, 255));
        jLabel86.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel86.setText("Approved");

        javax.swing.GroupLayout loginBtnQuanLayout = new javax.swing.GroupLayout(loginBtnQuan);
        loginBtnQuan.setLayout(loginBtnQuanLayout);
        loginBtnQuanLayout.setHorizontalGroup(
            loginBtnQuanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel86, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        loginBtnQuanLayout.setVerticalGroup(
            loginBtnQuanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel86, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        closeBtn.add(loginBtnQuan);
        loginBtnQuan.setBounds(220, 210, 160, 50);

        jLabel87.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel87.setText("Input Supervisor Pin #:");
        closeBtn.add(jLabel87);
        jLabel87.setBounds(90, 70, 220, 24);

        pinPassQuan.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        pinPassQuan.setBorder(null);
        pinPassQuan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pinPassQuanKeyReleased(evt);
            }
        });
        closeBtn.add(pinPassQuan);
        pinPassQuan.setBounds(90, 100, 430, 50);
        closeBtn.add(jSeparator15);
        jSeparator15.setBounds(90, 150, 430, 20);

        loginMain3.add(closeBtn, "card2");

        jPanel76.add(loginMain3, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout supervisorLoginFrameQuantityLayout = new javax.swing.GroupLayout(supervisorLoginFrameQuantity.getContentPane());
        supervisorLoginFrameQuantity.getContentPane().setLayout(supervisorLoginFrameQuantityLayout);
        supervisorLoginFrameQuantityLayout.setHorizontalGroup(
            supervisorLoginFrameQuantityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        supervisorLoginFrameQuantityLayout.setVerticalGroup(
            supervisorLoginFrameQuantityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel76, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1350, 730));
        setUndecorated(true);

        panel.setLayout(new java.awt.BorderLayout());

        jPanel11.setBackground(new java.awt.Color(25, 51, 76));
        jPanel11.setPreferredSize(new java.awt.Dimension(1361, 20));

        jLabel5.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("X");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGap(0, 1330, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
        );

        panel.add(jPanel11, java.awt.BorderLayout.PAGE_START);

        jPanel1.setBackground(new java.awt.Color(25, 51, 76));
        jPanel1.setMinimumSize(new java.awt.Dimension(350, 100));
        jPanel1.setPreferredSize(new java.awt.Dimension(350, 741));

        jPanel13.setBackground(new java.awt.Color(50, 76, 101));

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/user_profile.png"))); // NOI18N

        name.setFont(new java.awt.Font("Roboto Light", 1, 16)); // NOI18N
        name.setForeground(new java.awt.Color(255, 255, 255));

        jLabel25.setFont(new java.awt.Font("Roboto Light", 0, 13)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Cashier");

        cashierId.setFont(new java.awt.Font("Roboto Light", 0, 13)); // NOI18N
        cashierId.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel24)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cashierId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(name, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(name, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cashierId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(50, 76, 101));

        jLabel4.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Transaction #:");

        transNo.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        transNo.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Invoice ID:");

        invoiceId.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        invoiceId.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(transNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(invoiceId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(transNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(invoiceId, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(50, 76, 101));

        jLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Customer ID:");

        customerId.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        customerId.setForeground(new java.awt.Color(255, 255, 255));

        jLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Name:");

        customerName.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        customerName.setForeground(new java.awt.Color(255, 255, 255));

        jLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Points:");

        customerPoints.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        customerPoints.setForeground(new java.awt.Color(255, 255, 255));
        customerPoints.setText("0.0");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(customerId, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addComponent(customerName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customerPoints, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(25, 25, 25))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customerId, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customerName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customerPoints, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(50, 76, 101));

        jLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Barcode:");

        barcode1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        barcode1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel16.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Product Name:");

        prodName1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        prodName1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel18.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Description:");

        desc1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        desc1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel20.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Quantity:");

        qty1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        qty1.setForeground(new java.awt.Color(255, 255, 255));
        qty1.setText("0");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prodName1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(barcode1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(qty1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(desc1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barcode1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prodName1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(desc1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qty1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(50, 76, 101));

        jLabel22.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Product Information");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
        );

        jPanel12.setBackground(new java.awt.Color(50, 76, 101));
        jPanel12.setPreferredSize(new java.awt.Dimension(350, 42));

        jLabel23.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Customer Information");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(50, 76, 101));

        jLabel1.setFont(new java.awt.Font("Roboto Light", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Total");

        total.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        total.setForeground(new java.awt.Color(255, 255, 255));
        total.setText("P0.0");
        total.setPreferredSize(new java.awt.Dimension(335, 64));

        jPanel5.setBackground(new java.awt.Color(0, 204, 102));

        jLabel3.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Pay");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jPanel18.setBackground(new java.awt.Color(50, 76, 101));

        jLabel26.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel26.setText("2017/32/2");
        jLabel26.setToolTipText("");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel.add(jPanel1, java.awt.BorderLayout.LINE_END);

        jPanel3.setBackground(new java.awt.Color(25, 51, 76));
        jPanel3.setPreferredSize(new java.awt.Dimension(970, 90));

        jPanel23.setBackground(new java.awt.Color(25, 51, 76));

        jPanel14.setBackground(new java.awt.Color(50, 76, 101));
        jPanel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel14MouseClicked(evt);
            }
        });

        jPanel15.setBackground(new java.awt.Color(41, 83, 124));
        jPanel15.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel7.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("F1");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel19.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Transaction");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel39.setBackground(new java.awt.Color(50, 76, 101));
        jPanel39.setPreferredSize(new java.awt.Dimension(120, 34));

        jPanel40.setBackground(new java.awt.Color(41, 83, 124));
        jPanel40.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel27.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("F2");

        javax.swing.GroupLayout jPanel40Layout = new javax.swing.GroupLayout(jPanel40);
        jPanel40.setLayout(jPanel40Layout);
        jPanel40Layout.setHorizontalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel40Layout.setVerticalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel28.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Customer");

        javax.swing.GroupLayout jPanel39Layout = new javax.swing.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel39Layout.createSequentialGroup()
                .addComponent(jPanel40, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel28)
                .addContainerGap())
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel40, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel41.setBackground(new java.awt.Color(50, 76, 101));
        jPanel41.setPreferredSize(new java.awt.Dimension(120, 34));

        jPanel42.setBackground(new java.awt.Color(41, 83, 124));
        jPanel42.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel46.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("F3");

        javax.swing.GroupLayout jPanel42Layout = new javax.swing.GroupLayout(jPanel42);
        jPanel42.setLayout(jPanel42Layout);
        jPanel42Layout.setHorizontalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel42Layout.setVerticalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel47.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 255, 255));
        jLabel47.setText("Coupon");

        javax.swing.GroupLayout jPanel41Layout = new javax.swing.GroupLayout(jPanel41);
        jPanel41.setLayout(jPanel41Layout);
        jPanel41Layout.setHorizontalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel41Layout.createSequentialGroup()
                .addComponent(jPanel42, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel47)
                .addContainerGap())
        );
        jPanel41Layout.setVerticalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel42, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel43.setBackground(new java.awt.Color(50, 76, 101));
        jPanel43.setPreferredSize(new java.awt.Dimension(120, 34));

        jPanel44.setBackground(new java.awt.Color(41, 83, 124));
        jPanel44.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel48.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("F4");

        javax.swing.GroupLayout jPanel44Layout = new javax.swing.GroupLayout(jPanel44);
        jPanel44.setLayout(jPanel44Layout);
        jPanel44Layout.setHorizontalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel44Layout.setVerticalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel49.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setText("Logout");

        javax.swing.GroupLayout jPanel43Layout = new javax.swing.GroupLayout(jPanel43);
        jPanel43.setLayout(jPanel43Layout);
        jPanel43Layout.setHorizontalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addComponent(jPanel44, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel49)
                .addContainerGap())
        );
        jPanel43Layout.setVerticalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel44, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel45.setBackground(new java.awt.Color(50, 76, 101));
        jPanel45.setPreferredSize(new java.awt.Dimension(185, 34));

        jPanel46.setBackground(new java.awt.Color(41, 83, 124));
        jPanel46.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel50.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("F5");

        javax.swing.GroupLayout jPanel46Layout = new javax.swing.GroupLayout(jPanel46);
        jPanel46.setLayout(jPanel46Layout);
        jPanel46Layout.setHorizontalGroup(
            jPanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel46Layout.setVerticalGroup(
            jPanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel51.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("Pending Transaction");

        javax.swing.GroupLayout jPanel45Layout = new javax.swing.GroupLayout(jPanel45);
        jPanel45.setLayout(jPanel45Layout);
        jPanel45Layout.setHorizontalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel45Layout.createSequentialGroup()
                .addComponent(jPanel46, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
        );
        jPanel45Layout.setVerticalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel46, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel47.setBackground(new java.awt.Color(50, 76, 101));
        jPanel47.setPreferredSize(new java.awt.Dimension(185, 34));

        jPanel48.setBackground(new java.awt.Color(41, 83, 124));
        jPanel48.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel52.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("F6");

        javax.swing.GroupLayout jPanel48Layout = new javax.swing.GroupLayout(jPanel48);
        jPanel48.setLayout(jPanel48Layout);
        jPanel48Layout.setHorizontalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel48Layout.setVerticalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel53.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel53.setForeground(new java.awt.Color(255, 255, 255));
        jLabel53.setText("Void Transaction");

        javax.swing.GroupLayout jPanel47Layout = new javax.swing.GroupLayout(jPanel47);
        jPanel47.setLayout(jPanel47Layout);
        jPanel47Layout.setHorizontalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel47Layout.createSequentialGroup()
                .addComponent(jPanel48, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
        );
        jPanel47Layout.setVerticalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel48, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel49.setBackground(new java.awt.Color(50, 76, 101));
        jPanel49.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel50.setBackground(new java.awt.Color(41, 83, 124));
        jPanel50.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel54.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(255, 255, 255));
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("ESC");

        javax.swing.GroupLayout jPanel50Layout = new javax.swing.GroupLayout(jPanel50);
        jPanel50.setLayout(jPanel50Layout);
        jPanel50Layout.setHorizontalGroup(
            jPanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel50Layout.setVerticalGroup(
            jPanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel55.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(255, 255, 255));
        jLabel55.setText("Clear Selection");

        javax.swing.GroupLayout jPanel49Layout = new javax.swing.GroupLayout(jPanel49);
        jPanel49.setLayout(jPanel49Layout);
        jPanel49Layout.setHorizontalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addComponent(jPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE))
        );
        jPanel49Layout.setVerticalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel50, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel51.setBackground(new java.awt.Color(50, 76, 101));
        jPanel51.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel52.setBackground(new java.awt.Color(41, 83, 124));
        jPanel52.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel56.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(255, 255, 255));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("DEL");

        javax.swing.GroupLayout jPanel52Layout = new javax.swing.GroupLayout(jPanel52);
        jPanel52.setLayout(jPanel52Layout);
        jPanel52Layout.setHorizontalGroup(
            jPanel52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );
        jPanel52Layout.setVerticalGroup(
            jPanel52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel57.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel57.setForeground(new java.awt.Color(255, 255, 255));
        jLabel57.setText("Remove Item");

        javax.swing.GroupLayout jPanel51Layout = new javax.swing.GroupLayout(jPanel51);
        jPanel51.setLayout(jPanel51Layout);
        jPanel51Layout.setHorizontalGroup(
            jPanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel51Layout.createSequentialGroup()
                .addComponent(jPanel52, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE))
        );
        jPanel51Layout.setVerticalGroup(
            jPanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel52, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel53.setBackground(new java.awt.Color(50, 76, 101));
        jPanel53.setPreferredSize(new java.awt.Dimension(120, 34));

        jPanel54.setBackground(new java.awt.Color(41, 83, 124));
        jPanel54.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel58.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel58.setForeground(new java.awt.Color(255, 255, 255));
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("ENTER");

        javax.swing.GroupLayout jPanel54Layout = new javax.swing.GroupLayout(jPanel54);
        jPanel54.setLayout(jPanel54Layout);
        jPanel54Layout.setHorizontalGroup(
            jPanel54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel54Layout.createSequentialGroup()
                .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel54Layout.setVerticalGroup(
            jPanel54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel58, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel59.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel59.setForeground(new java.awt.Color(255, 255, 255));
        jLabel59.setText("Select");

        javax.swing.GroupLayout jPanel53Layout = new javax.swing.GroupLayout(jPanel53);
        jPanel53.setLayout(jPanel53Layout);
        jPanel53Layout.setHorizontalGroup(
            jPanel53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel53Layout.createSequentialGroup()
                .addComponent(jPanel54, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel53Layout.setVerticalGroup(
            jPanel53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel54, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel55.setBackground(new java.awt.Color(50, 76, 101));
        jPanel55.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel56.setBackground(new java.awt.Color(41, 83, 124));
        jPanel56.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel60.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel60.setForeground(new java.awt.Color(255, 255, 255));
        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel60.setText("F7");

        javax.swing.GroupLayout jPanel56Layout = new javax.swing.GroupLayout(jPanel56);
        jPanel56.setLayout(jPanel56Layout);
        jPanel56Layout.setHorizontalGroup(
            jPanel56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );
        jPanel56Layout.setVerticalGroup(
            jPanel56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel61.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel61.setForeground(new java.awt.Color(255, 255, 255));
        jLabel61.setText("Pending List");

        javax.swing.GroupLayout jPanel55Layout = new javax.swing.GroupLayout(jPanel55);
        jPanel55.setLayout(jPanel55Layout);
        jPanel55Layout.setHorizontalGroup(
            jPanel55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel55Layout.createSequentialGroup()
                .addComponent(jPanel56, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel55Layout.setVerticalGroup(
            jPanel55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel56, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel57.setBackground(new java.awt.Color(50, 76, 101));
        jPanel57.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel58.setBackground(new java.awt.Color(41, 83, 124));
        jPanel58.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel62.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel62.setForeground(new java.awt.Color(255, 255, 255));
        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel62.setText("F8");

        javax.swing.GroupLayout jPanel58Layout = new javax.swing.GroupLayout(jPanel58);
        jPanel58.setLayout(jPanel58Layout);
        jPanel58Layout.setHorizontalGroup(
            jPanel58Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel62, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );
        jPanel58Layout.setVerticalGroup(
            jPanel58Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel62, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel63.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel63.setForeground(new java.awt.Color(255, 255, 255));
        jLabel63.setText("Update Quantity");

        javax.swing.GroupLayout jPanel57Layout = new javax.swing.GroupLayout(jPanel57);
        jPanel57.setLayout(jPanel57Layout);
        jPanel57Layout.setHorizontalGroup(
            jPanel57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel57Layout.createSequentialGroup()
                .addComponent(jPanel58, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel57Layout.setVerticalGroup(
            jPanel57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel58, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel61.setBackground(new java.awt.Color(50, 76, 101));
        jPanel61.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel62.setBackground(new java.awt.Color(41, 83, 124));
        jPanel62.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel64.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel64.setForeground(new java.awt.Color(255, 255, 255));
        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel64.setText("F9");

        javax.swing.GroupLayout jPanel62Layout = new javax.swing.GroupLayout(jPanel62);
        jPanel62.setLayout(jPanel62Layout);
        jPanel62Layout.setHorizontalGroup(
            jPanel62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel64, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );
        jPanel62Layout.setVerticalGroup(
            jPanel62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel64, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel65.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel65.setForeground(new java.awt.Color(255, 255, 255));
        jLabel65.setText("Records");

        javax.swing.GroupLayout jPanel61Layout = new javax.swing.GroupLayout(jPanel61);
        jPanel61.setLayout(jPanel61Layout);
        jPanel61Layout.setHorizontalGroup(
            jPanel61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel61Layout.createSequentialGroup()
                .addComponent(jPanel62, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel65)
                .addContainerGap())
        );
        jPanel61Layout.setVerticalGroup(
            jPanel61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel62, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel63.setBackground(new java.awt.Color(50, 76, 101));
        jPanel63.setPreferredSize(new java.awt.Dimension(160, 34));

        jPanel64.setBackground(new java.awt.Color(41, 83, 124));
        jPanel64.setPreferredSize(new java.awt.Dimension(20, 20));

        jLabel68.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel68.setForeground(new java.awt.Color(255, 255, 255));
        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel68.setText("F10");

        javax.swing.GroupLayout jPanel64Layout = new javax.swing.GroupLayout(jPanel64);
        jPanel64.setLayout(jPanel64Layout);
        jPanel64Layout.setHorizontalGroup(
            jPanel64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel68, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );
        jPanel64Layout.setVerticalGroup(
            jPanel64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel68, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jLabel69.setFont(new java.awt.Font("Roboto Light", 1, 12)); // NOI18N
        jLabel69.setForeground(new java.awt.Color(255, 255, 255));
        jLabel69.setText("Change Pass");

        javax.swing.GroupLayout jPanel63Layout = new javax.swing.GroupLayout(jPanel63);
        jPanel63.setLayout(jPanel63Layout);
        jPanel63Layout.setHorizontalGroup(
            jPanel63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel63Layout.createSequentialGroup()
                .addComponent(jPanel64, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel63Layout.setVerticalGroup(
            jPanel63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel64, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
            .addComponent(jLabel69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(jPanel49, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel51, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel53, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel55, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel61, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel63, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE))
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        mainTransactionPanel.setLayout(new java.awt.CardLayout());

        transactionPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel29.setBackground(new java.awt.Color(255, 255, 255));

        quantity.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        quantity.setText("1");
        quantity.registerKeyboardAction(null, KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        quantity.setMargin(new java.awt.Insets(2, 10, 2, 2));
        quantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quantityActionPerformed(evt);
            }
        });
        quantity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                quantityKeyReleased(evt);
            }
        });

        searchProduct.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        searchProduct.setMargin(new java.awt.Insets(2, 10, 2, 2));
        searchProduct.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                searchProductFocusGained(evt);
            }
        });
        searchProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchProductKeyReleased(evt);
            }
        });

        jPanel21.setBackground(new java.awt.Color(50, 76, 101));

        jLabel33.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("Add Item");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(searchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                    .addComponent(quantity)
                    .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        mainPanel.setLayout(new java.awt.CardLayout());

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        transTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(50,76,101)));
        transTable.setModel(CustomTableModel.transactionTableModel);
        CustomTableModel.initializeTransactionTableColumn();
        transTable.getColumn(0).setPreferredWidth(30);
        transTable.getColumn(1).setPreferredWidth(150);
        transTable.getColumn(2).setPreferredWidth(200);
        transTable.getColumn(3).setPreferredWidth(150);
        transTable.getColumn(4).setPreferredWidth(70);
        transTable.getColumn(5).setPreferredWidth(70);
        transTable.getColumn(6).setPreferredWidth(100);
        transTable.setEditable(false);
        transTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        transTable.setRowHeight(30);
        transTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                transTableMouseReleased(evt);
            }
        });
        transTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                transTableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(transTable);

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 983, Short.MAX_VALUE)
            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 983, Short.MAX_VALUE))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 559, Short.MAX_VALUE)
            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE))
        );

        mainPanel.add(panel1, "card2");

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));

        productTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(50,76,101)));
        productTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        productTable.setEditable(false);
        productTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        productTable.setRowHeight(30);
        productTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                productTableKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(productTable);

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 983, Short.MAX_VALUE)
            .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 983, Short.MAX_VALUE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 559, Short.MAX_VALUE)
            .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE))
        );

        mainPanel.add(panel2, "card2");

        javax.swing.GroupLayout transactionPanelLayout = new javax.swing.GroupLayout(transactionPanel);
        transactionPanel.setLayout(transactionPanelLayout);
        transactionPanelLayout.setHorizontalGroup(
            transactionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(transactionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        transactionPanelLayout.setVerticalGroup(
            transactionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transactionPanelLayout.createSequentialGroup()
                .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainTransactionPanel.add(transactionPanel, "card2");

        pendingPanel.setBackground(new java.awt.Color(255, 255, 255));

        pendingTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(50,76,101)));
        pendingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        pendingTable.setEditable(false);
        pendingTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        pendingTable.setRowHeight(30);
        pendingTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pendingTableKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(pendingTable);

        text.setFont(new java.awt.Font("Roboto Light", 0, 36)); // NOI18N
        text.setText("Pending List");
        text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout pendingPanelLayout = new javax.swing.GroupLayout(pendingPanel);
        pendingPanel.setLayout(pendingPanelLayout);
        pendingPanelLayout.setHorizontalGroup(
            pendingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pendingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pendingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(pendingPanelLayout.createSequentialGroup()
                        .addComponent(text, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 657, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pendingPanelLayout.setVerticalGroup(
            pendingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pendingPanelLayout.createSequentialGroup()
                .addComponent(text, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 572, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainTransactionPanel.add(pendingPanel, "card2");

        jPanel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jPanelKeyReleased(evt);
            }
        });
        jPanel.setLayout(new java.awt.BorderLayout());
        mainTransactionPanel.add(jPanel, "card4");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1003, Short.MAX_VALUE)
            .addComponent(mainTransactionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(mainTransactionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panel.add(jPanel2, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, 1353, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void quantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quantityActionPerformed

    private void quantityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_quantityKeyReleased
        try{
            Integer.parseInt(quantity.getText());

            cashier.setProductInfo(searchProduct.getText());
            barcode1.setText(cashier.getProductBarcode());
            prodName1.setText(cashier.getProductBrandName());
            desc1.setText(cashier.getProductDesc());
            qty1.setText(String.valueOf(cashier.getProductQuantity()));

            switchToPanel(mainPanel, panel1);

            if (evt.getKeyCode() == 10 && save) {
                if (Integer.parseInt(quantity.getText()) <= Integer.parseInt(qty1.getText())) {
                    boolean exist = false;
                    
                    for (int i = 0; i < transTable.getRowCount(); i++) {
                        if (cashier.getProductBarcode().equals(transTable.getValueAt(i, 1).toString())) {
                            exist = true;
                            int totalQty = Integer.parseInt(transTable.getValueAt(i, 5).toString()) + Integer.parseInt(quantity.getText());
                            String price = transTable.getValueAt(i, 4).toString();
                            subTotal = Double.parseDouble(price.substring(1, price.length())) * totalQty;                            
                            transTable.setEditable(true);
                            transTable.setValueAt(totalQty, i, 5);
                            transTable.setValueAt("P" + subTotal, i, 6);
                            transTable.setEditable(false);
                        }
                    }
                    
                     
                       if(couponStatus)
                       {
                            System.out.println("SHAKS");
                            subTotal1 = cashier.couponCode(barcode1.getText()) * Integer.parseInt(quantity.getText());
                            
                            
                       }
                       else if(cashier.checkBarcodePromo(barcode1.getText()))
                       {
                           
                            subTotal1 = cashier.discountItem(barcode1.getText()) * Integer.parseInt(quantity.getText());
                       }
                       else{
                        
                        subTotal1 = cashier.getProductPrice() * Integer.parseInt(quantity.getText());
                    }
                    tot = tot + subTotal1;
                    total.setText("P" + tot);
                    
                    if (!exist) {
                        iterate++;
                        CustomTableModel.transactionTableModel.addRow(new Object[]{iterate, cashier.getProductBarcode(), cashier.getProductName(), cashier.getProductBrandName(), "P" + cashier.getProductPrice(), quantity.getText(), "P" + subTotal});
                    }
                    
                    clearFields();
                }
                else{
                    popUp.showMessage("ERROR: Quantity is not enough!", 1);
                }
            }
            else if(evt.getKeyCode() == 116){
                pending();
            }
            
            save = true;
        }
        catch(NumberFormatException ex){
            quantity.setText("");
        }
    }//GEN-LAST:event_quantityKeyReleased

    private void searchProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchProductKeyReleased
        if (cashier.isSearch()) {
            if(searchProduct.getText().isEmpty()){
                switchToPanel(mainPanel, panel1);
                if(evt.getKeyCode() == 40){
                    transTable.requestFocusInWindow();
                }   
            }
            
            if (evt.getKeyCode() == 40) {
                productTable.requestFocusInWindow();
            }
            else if(evt.getKeyCode() == 112){
                searchProduct.setText("");
                searchProduct.requestFocusInWindow();
                switchToPanel(mainTransactionPanel, transactionPanel);
            }
            else if(evt.getKeyCode() == 10){
                if(searchProduct.getText().isEmpty() && transTable.getRowCount() >= 1 && !pending){
                    total2.setText(total.getText());
                    payFrame.setLocationRelativeTo(null);
                    payFrame.setPreferredSize(new Dimension(720, 565));
                    inputAmount1.requestFocusInWindow();
                    payFrame.setVisible(true);
                }
                else{
                    pending = false;
                }
                
                switchToPanel(mainPanel, panel2);
                cashier.searchProduct(productTable, searchProduct.getText());
                boolean check = cashier.checkBarcode(searchProduct.getText());
                
                if(!check && !searchProduct.getText().isEmpty() && productTable.getRowCount() <= 0){
                    popUp.showMessage("Barcode doesn't exist", 1);
                }
                
                if(check)
                    quantity.requestFocusInWindow();
            }
            else if(evt.getKeyCode() == 117){
                clearTransaction();
                searchProduct.requestFocusInWindow();
            }
            else if(evt.getKeyCode() == 115){
                dispose();
            }
            else if(evt.getKeyCode() == 116){
                pending();
                searchProduct.requestFocusInWindow();
            }
            else if(evt.getKeyCode() == 113){
                pointIdFrame.setModal(true);
                pointIdFrame.setResizable(false);
                pointIdFrame.setLocationRelativeTo(null);
                pointIdFrame.setVisible(true);
                
            }
            else if(evt.getKeyCode() == 114){
                couponFrame.setModal(true);
                couponFrame.setResizable(false);
                couponFrame.setLocationRelativeTo(null);
                couponFrame.setVisible(true);
                
            }
            else if(evt.getKeyCode() == 118){
                switchToPanel(mainTransactionPanel, pendingPanel);
                cashier.loadPendingList(pendingTable, cashierId.getText());
                text.requestFocus();
            }
            else if(evt.getKeyCode() == 27){
                searchProduct.setText("");
                searchProduct.requestFocusInWindow();
            }
          System.out.println(evt);
        }
    }//GEN-LAST:event_searchProductKeyReleased

    private void productTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_productTableKeyReleased
        cashier.setSelectedRow(productTable.getSelectedRow());
        cashier.setSelectedColumn(productTable.getSelectedColumn());
        
        System.out.println(cashier.getSelectedRow());
        System.out.println(cashier.getSelectedColumn());
        if(evt.getKeyCode() == 27){
            productTable.clearSelection();
            searchProduct.requestFocusInWindow();
        }
    }//GEN-LAST:event_productTableKeyReleased

    private void searchProductFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_searchProductFocusGained
        cashier.setSearch(true);
        if(searchProduct.getText().isEmpty()){
            switchToPanel(mainPanel, panel1);
        }
        save = false;
        if(authenticate){
            login.setModal(true);
            login.setLocationRelativeTo(this);
            login.setVisible(true);
        }
    }//GEN-LAST:event_searchProductFocusGained

    private void transTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_transTableKeyReleased
        row = transTable.getSelectedRow();
        col = transTable.getSelectedColumn();
        if(evt.getKeyCode() == 27){
            transTable.clearSelection();
            searchProduct.requestFocusInWindow();
        }
        else if(evt.getKeyCode() == 121){
            if(transTable.getSelectedRow() > -1){
                updateQty.setLocationRelativeTo(this);
                updateQty.setVisible(true);
                minus.setText("");
            }
        }
        else if(evt.getKeyCode() == 127){
            if(transTable.getSelectedRow() != -1){
                supervisorLoginFrame.setLocationRelativeTo(this);
                supervisorLoginFrame.setVisible(true);
            }
        }
        System.out.println(evt.getKeyCode()+" "+evt.getKeyChar());
    }//GEN-LAST:event_transTableKeyReleased

    private void pendingTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pendingTableKeyReleased
        if(evt.getKeyCode() == 112){
            searchProduct.setText("");
            searchProduct.requestFocusInWindow(); 
            switchToPanel(mainTransactionPanel, transactionPanel);
        }
    }//GEN-LAST:event_pendingTableKeyReleased

    private void textKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textKeyReleased
        pendingTable.requestFocusInWindow();
        if(evt.getKeyCode() == 112){
            searchProduct.setText("");
            searchProduct.requestFocusInWindow();
            switchToPanel(mainTransactionPanel, transactionPanel);
        }
    }//GEN-LAST:event_textKeyReleased

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        if(success){
            clearAll();
            success = false;
        }
        payFrame.dispose();
    }//GEN-LAST:event_jLabel9MouseClicked

    private void printBtn1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_printBtn1KeyReleased
        if(evt.getKeyCode() == 10){
            clearAll();
        }
        payFrame.dispose();
    }//GEN-LAST:event_printBtn1KeyReleased

    private void inputAmount1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputAmount1KeyReleased
        try {
            Double.parseDouble(inputAmount1.getText());
            switch (evt.getKeyCode()) {
                case 10:
                    String t = total.getText();
                    double tot = Double.parseDouble(t.substring(1, t.length()));
                    double amount = Double.parseDouble(inputAmount1.getText());
                    if (amount >= tot) {
                        double total = amount - tot;
                        changeValue1.setText("P" + String.valueOf(total));
                        switchToPanel(main1, change1);
                        success = true;
                        for(int i = 0; i < transTable.getRowCount(); i++){
                            String barcode = transTable.getValueAt(i, 1).toString();
                            int qty = Integer.parseInt(transTable.getValueAt(i, 5).toString());
                            cashier.deductProductQty(barcode, qty);
                        }
                        save();
                        view = new JRViewer(reports.printOrders(invoiceId.getText()));
                        jPanel.repaint();
                        jPanel.add(view);
                        jPanel.revalidate();
                        jPanel.requestFocusInWindow();
                        switchToPanel(mainTransactionPanel, jPanel);
                        msg1.setText("");
                        printBtn1.setText("Done");
                        printBtn1.requestFocusInWindow();
                    } else {
                        msg1.setVisible(true);
                        msg1.setText("ERROR: Amount is not enough!");
                    }
                    break;
                default:
                    break;
            }
            System.out.println(evt);
        } catch (NumberFormatException numberFormatException) {
            inputAmount1.setText("");
            if(evt.getKeyCode() == 27){
                payFrame.setVisible(false);
            }
        }
    }//GEN-LAST:event_inputAmount1KeyReleased

    private void jPanel32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel32MouseClicked
       try{
       ResultSet rs = cashier.checkCustId(inputCustId.getText());
       if(rs.next())
       {
          custId = rs.getString("customer_id");
          lastName = rs.getString("last_name");
          midName = rs.getString("middle_name");
          firstName = rs.getString("first_name");       }
          
      }
       catch (SQLException ex) {
       }
       if (!custId.isEmpty() && !lastName.isEmpty() && !midName.isEmpty() && !firstName.isEmpty())
       {
          
           customerId.setText(custId);
           customerName.setText(lastName+", "+midName+" "+firstName);
           cusNamePayLabel.setText(lastName+", "+midName+" "+firstName);
           customerPoints.setText(cashier.getCustPoint(custId));
           pointIdFrame.dispose();
       }
        
    }//GEN-LAST:event_jPanel32MouseClicked

    private void jPanel33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel33MouseClicked
        pointIdFrame.dispose();
    }//GEN-LAST:event_jPanel33MouseClicked

    private void inputCustIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputCustIdKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_inputCustIdKeyReleased

    private void jLabel29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MouseClicked
        login.setVisible(false);
        dispose();
    }//GEN-LAST:event_jLabel29MouseClicked

    private void loginBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnMouseClicked
        loginId.setText("");
        loginPass.setText("");
        login();
    }//GEN-LAST:event_loginBtnMouseClicked

    private void registerBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_registerBtnMouseClicked
        register();
        registerId.setText("");
        registerPassword.setText("");
        registerPassword1.setText("");
    }//GEN-LAST:event_registerBtnMouseClicked

    private void loginBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBackMouseClicked
        switchToPanel(loginMain, loginPanel);
        clearLoginForm();
    }//GEN-LAST:event_loginBackMouseClicked

    private void createAccountBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_createAccountBtnMouseClicked
        switchToPanel(loginMain, registerPanel);
        clearRegisterForm();
    }//GEN-LAST:event_createAccountBtnMouseClicked

    private void loginIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginIdKeyReleased
        if(evt.getKeyCode() == 10){
            login();
            searchProduct.requestFocusInWindow();
        }
    }//GEN-LAST:event_loginIdKeyReleased

    private void loginPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginPassKeyReleased
        if(evt.getKeyCode() == 10){
            login();
            searchProduct.requestFocusInWindow();
        }
    }//GEN-LAST:event_loginPassKeyReleased

    private void registerIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_registerIdKeyReleased
        if(evt.getKeyCode() == 10){
            register();
        }
    }//GEN-LAST:event_registerIdKeyReleased

    private void registerPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_registerPasswordKeyReleased
        if(evt.getKeyCode() == 10){
            register();
        }
    }//GEN-LAST:event_registerPasswordKeyReleased

    private void registerPassword1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_registerPassword1KeyReleased
        if(evt.getKeyCode() == 10){
            register();
        }
    }//GEN-LAST:event_registerPassword1KeyReleased

    private void updateQuantityMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateQuantityMouseClicked
        if(!add.getText().isEmpty()){
            updateQuantity();
        }
        else if(!minus.getText().isEmpty()){
            supervisorLoginFrameQuantity.setVisible(true);
            supervisorLoginFrameQuantity.setLocationRelativeTo(this);
        }
    }//GEN-LAST:event_updateQuantityMouseClicked

    private void transTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_transTableMouseReleased
        row = transTable.getSelectedRow();
        col = transTable.getSelectedColumn();
    }//GEN-LAST:event_transTableMouseReleased

    private void addFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_addFocusLost
        add.setText("");
    }//GEN-LAST:event_addFocusLost

    private void minusFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_minusFocusLost
        //minus.setText("");
    }//GEN-LAST:event_minusFocusLost

    private void jLabel71MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel71MouseClicked
        updateQty.setVisible(false);
    }//GEN-LAST:event_jLabel71MouseClicked

    private void addKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_addKeyReleased
        if(evt.getKeyCode() == 10){
            updateQuantity();
        }
    }//GEN-LAST:event_addKeyReleased

    private void minusKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_minusKeyReleased
        if(evt.getKeyCode() == 10){
            supervisorLoginFrameQuantity.setVisible(true);
            supervisorLoginFrameQuantity.setLocationRelativeTo(this);
        }
    }//GEN-LAST:event_minusKeyReleased

    private void jPanelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanelKeyReleased
        
    }//GEN-LAST:event_jPanelKeyReleased

    private void jPanel14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel14MouseClicked
        searchProduct.setText("");
        switchToPanel(mainTransactionPanel, transactionPanel);
        searchProduct.requestFocusInWindow();
    }//GEN-LAST:event_jPanel14MouseClicked

    private void jLabel72MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel72MouseClicked
        supervisorLoginFrame.setVisible(false);
    }//GEN-LAST:event_jLabel72MouseClicked

    private void pinPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pinPassKeyReleased
        if(evt.getKeyCode() == 10){
            deleteApprove();
            pinPass.setText("");
        }
    }//GEN-LAST:event_pinPassKeyReleased

    private void loginBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtn1MouseClicked
        deleteApprove();
        pinPass.setText("");
    }//GEN-LAST:event_loginBtn1MouseClicked

    private void jLabel83MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel83MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel83MouseClicked

    private void closeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeMouseClicked
        list.setVisible(false);
    }//GEN-LAST:event_closeMouseClicked

    private void jLabel84MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel84MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel84MouseClicked

    private void cancelBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelBtnMouseClicked
        list.setVisible(false);
    }//GEN-LAST:event_cancelBtnMouseClicked

    private void loginBtn1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginBtn1KeyPressed
        
    }//GEN-LAST:event_loginBtn1KeyPressed

    private void loginBtn1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginBtn1KeyReleased
        
    }//GEN-LAST:event_loginBtn1KeyReleased

    private void jPanel74MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel74MouseClicked
        if(cashier.checkCouponCode(inputCoupCode.getText())){
               couponStatus=true; 
               couponFrame.dispose();
               popUp.showMessage("Successfully Activated Coupon!", 0);
               searchProduct.requestFocusInWindow();
           }
           else{
               popUp.showMessage("ERROR: Coupon Code does not exist!", 1);
               inputCoupCode.setText("");
           }        
    }//GEN-LAST:event_jPanel74MouseClicked

    private void jPanel75MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel75MouseClicked
        couponFrame.dispose();                // TODO add your handling code here:
    }//GEN-LAST:event_jPanel75MouseClicked

    private void inputCoupCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputCoupCodeKeyReleased
        if(evt.getKeyCode() == 10){
            
           if(cashier.checkCouponCode(inputCoupCode.getText())){
               couponStatus=true; 
               couponFrame.dispose();
               popUp.showMessage("Successfully Activated Coupon!", 0);
               searchProduct.requestFocusInWindow();
           }
           else{
               popUp.showMessage("ERROR: Coupon Code does not exist!", 1);
               inputCoupCode.setText("");
           }
        }        
    }//GEN-LAST:event_inputCoupCodeKeyReleased

    private void closeBtnQuanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBtnQuanMouseClicked
        supervisorLoginFrameQuantity.setVisible(false);
    }//GEN-LAST:event_closeBtnQuanMouseClicked

    private void jLabel85MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel85MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel85MouseClicked

    private void loginBtnQuanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnQuanMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_loginBtnQuanMouseClicked

    private void loginBtnQuanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginBtnQuanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_loginBtnQuanKeyPressed

    private void loginBtnQuanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loginBtnQuanKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_loginBtnQuanKeyReleased

    private void pinPassQuanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pinPassQuanKeyReleased
        if(evt.getKeyCode() == 10){
            quantityDeleteApproval();
            pinPassQuan.setText("");
        }
    }//GEN-LAST:event_pinPassQuanKeyReleased
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CashierFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CashierFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CashierFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CashierFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CashierFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField add;
    private javax.swing.JPanel amt1;
    private javax.swing.JLabel barcode1;
    private javax.swing.JPanel cancelBtn;
    private javax.swing.JLabel cashierId;
    private javax.swing.JPanel change1;
    private javax.swing.JLabel changeValue1;
    private javax.swing.JLabel close;
    private javax.swing.JPanel closeBtn;
    private javax.swing.JLabel closeBtnQuan;
    private javax.swing.JDialog couponFrame;
    private javax.swing.JLabel createAccountBtn;
    private javax.swing.JLabel cusNamePayLabel;
    private javax.swing.JLabel customerId;
    private javax.swing.JLabel customerName;
    private javax.swing.JLabel customerPoints;
    private javax.swing.JLabel desc1;
    private javax.swing.JTextField inputAmount1;
    private javax.swing.JTextField inputCoupCode;
    private javax.swing.JTextField inputCustId;
    private javax.swing.JLabel invoiceId;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel45;
    private javax.swing.JPanel jPanel46;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel50;
    private javax.swing.JPanel jPanel51;
    private javax.swing.JPanel jPanel52;
    private javax.swing.JPanel jPanel53;
    private javax.swing.JPanel jPanel54;
    private javax.swing.JPanel jPanel55;
    private javax.swing.JPanel jPanel56;
    private javax.swing.JPanel jPanel57;
    private javax.swing.JPanel jPanel58;
    private javax.swing.JPanel jPanel59;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel60;
    private javax.swing.JPanel jPanel61;
    private javax.swing.JPanel jPanel62;
    private javax.swing.JPanel jPanel63;
    private javax.swing.JPanel jPanel64;
    private javax.swing.JPanel jPanel65;
    private javax.swing.JPanel jPanel66;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel71;
    private javax.swing.JPanel jPanel72;
    private javax.swing.JPanel jPanel73;
    private javax.swing.JPanel jPanel74;
    private javax.swing.JPanel jPanel75;
    private javax.swing.JPanel jPanel76;
    private javax.swing.JPanel jPanel77;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private static javax.swing.JDialog list;
    private static javax.swing.JDialog login;
    private javax.swing.JLabel loginBack;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtnQuan;
    private javax.swing.JTextField loginId;
    private javax.swing.JPanel loginMain;
    private javax.swing.JPanel loginMain1;
    private javax.swing.JPanel loginMain2;
    private javax.swing.JPanel loginMain3;
    private javax.swing.JPanel loginPanel;
    private javax.swing.JPasswordField loginPass;
    private javax.swing.JPanel main1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel mainTransactionPanel;
    private javax.swing.JTextField minus;
    private javax.swing.JLabel msg1;
    private javax.swing.JLabel name;
    private javax.swing.JPanel panel;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JDialog payFrame;
    private javax.swing.JPanel pendingPanel;
    private org.jdesktop.swingx.JXTable pendingTable;
    private javax.swing.JPasswordField pinPass;
    private javax.swing.JPasswordField pinPassQuan;
    private javax.swing.JDialog pointIdFrame;
    private javax.swing.JLabel pointsEarnedLabel;
    private javax.swing.JLabel printBtn1;
    private javax.swing.JLabel prodName1;
    private org.jdesktop.swingx.JXTable productTable;
    private javax.swing.JPanel prompt;
    private javax.swing.JLabel promptIcon;
    private javax.swing.JLabel promptMsg;
    private javax.swing.JTable qItem;
    private javax.swing.JLabel qty1;
    private javax.swing.JTextField quantity;
    private javax.swing.JPanel registerBtn;
    private javax.swing.JTextField registerId;
    private javax.swing.JPanel registerPanel;
    private javax.swing.JPanel registerPanel1;
    private javax.swing.JPanel registerPanel2;
    private javax.swing.JPasswordField registerPassword;
    private javax.swing.JPasswordField registerPassword1;
    private javax.swing.JTextField searchProduct;
    private static javax.swing.JDialog supervisorLoginFrame;
    private static javax.swing.JDialog supervisorLoginFrameQuantity;
    private javax.swing.JLabel text;
    private javax.swing.JLabel total;
    private javax.swing.JLabel total2;
    private javax.swing.JLabel transNo;
    private org.jdesktop.swingx.JXTable transTable;
    private javax.swing.JPanel transactionPanel;
    private javax.swing.JDialog updateQty;
    private javax.swing.JPanel updateQuantity;
    // End of variables declaration//GEN-END:variables
    
    private void initialize(){
        CustomTableModel.initializeProductTransTableColumn();
        CustomTableModel.initializePendingTableColumn();
        
        controller.placeHolder("Input Barcode", searchProduct);
        controller.placeHolder("Qty", quantity);
        controller.placeHolder("Add Qty", add);
        controller.placeHolder("Minus Qty", minus);
        
        couponStatus = false;
        popUp = new PopUpMessage(promptIcon, promptMsg, prompt, panel);
        createKeybindings(productTable);
        createKeybindings(pendingTable);
        generateInvoiceId();
    }
    
    private void switchToPanel(JPanel parent, JPanel panel){
        parent.removeAll();
        parent.add(panel);
        parent.revalidate();
        parent.repaint();
    }
  
    /**
     * Switching panels
     * @param panel a panel to switch to
     */
//    public void switchTo(JPanel panel){
//        main.removeAll();
//        main.add(panel);
//        main.revalidate();
//        main.repaint();
//    }
//    
//    public void switchTo2(JPanel panel){
//        mainOrderPanel.removeAll();
//        mainOrderPanel.add(panel);
//        mainOrderPanel.revalidate();
//        mainOrderPanel.repaint();
//    }
//    
//    /**
//     * Initialize all components in first run
//     */
//    private void initialize(){
//        placeHolder("Quantity", qty);
//        placeHolder("Barcode", prodCode);
//        qty.setBackground(null);
//        customizeTable();
//        scanBarcode();
//        generateInvoiceId();
//        clearFields();
//    }
//    
//    /**
//     * Clear all fields
//     */
//    private void clearFields(){
//        prodCode.setText("");
//        qty.setText("");
//    }
//    
//    /**
//     * Placing placeholders in every text field
//     * @param text hint on what to input in that field.
//     * @param field a textfield component.
//     */
//    private void placeHolder(String text, JTextField field){
//        TextPrompts placeholder = new TextPrompts(text, field);
//        placeholder.setForeground(new Color(120, 120, 120));
//        placeholder.setFont(new Font("Roboto Light", 0, 14));
//    }
//
//    /**
//     * Refreshing item table
//     */
//    private void refreshItemTable(){
//        if (itemId >= 0) {
//            double total = 0;
//            for(int i = 0; i < orderListTable.getRowCount(); i++){
//                double price = Double.parseDouble(orderListTable.getValueAt(i, 4).toString());
//                int qty = Integer.parseInt(orderListTable.getValueAt(i, 5).toString());
//                double tot = price * (double) qty;
//                orderListTable.setValueAt(tot, i, 6);
//                total = total + Double.parseDouble(orderListTable.getValueAt(i, 6).toString());
//            }
//            this.total.setText(String.valueOf(total));
//            prodCode.requestFocus();
//            orderListTable.clearSelection();
//            clearFields();
//        }
//    }
//    
//    /**
//     * Paint table header
//     */
//    private void customizeTable(){
//        for(String col : itemListCol){
//            model.addColumn(col);
//        }
//        orderListTable.setModel(model);
//        pendingTransactionTable.setModel(model1);
//        
//        orderListTable.getTableHeader().setDefaultRenderer(new DefaultHeaderRenderer(new Color(0,154,77)));
//        orderListTableScroll.getViewport().setBackground(new Color(255,255,255));
//        
//        searchProductTable.getTableHeader().setDefaultRenderer(new DefaultHeaderRenderer(new Color(0,154,77)));
//        searchProductListTableScroll.getViewport().setBackground(new Color(255,255,255));
//        
//        pendingTransactionTable.getTableHeader().setDefaultRenderer(new DefaultHeaderRenderer(new Color(0,154,77)));
//        pendTransTableScroll.getViewport().setBackground(new Color(255,255,255));
//    }
//    
//    /**
//     * Adds item in the table
//     */
//    private void addItem(){
//        if (!prodCode.getText().isEmpty()) {
//            boolean check = cashier.checkBarcode(prodCode.getText(), orderListTable);
//            if (!check) {
//                double subTotal = cashier.getSubTotal(Double.parseDouble(productInfo.get(4)), Integer.parseInt(qty.getText()));
//                cashier.addItem(model, productInfo, Integer.parseInt(qty.getText()), subTotal);
//                refreshItemTable();
//                clearFields();
//            } else {
//                int row = cashier.getRow(prodCode.getText(), orderListTable);
//                int curQty = cashier.getCurrentQty(row, orderListTable);
//                int val = (int) cashier.appendQty(curQty, Integer.parseInt(qty.getText()));
//                orderListTable.setValueAt(val, row, 5);
//                refreshItemTable();
//                clearFields();
//            }
//            isFocus = false;
//            isUsingBarcodeScanner = false;
//            prodCode.requestFocus();
//        }
//        else{
//            JOptionPane.showMessageDialog(null, "Barcode field is empty!");
//        }
//    }
//    
//    /**
//     * Detects when barcode reader scan
//     */
//    private void scanBarcode(){
//        barcode.addBarcodeListener(new BarcodeReader.BarcodeListener() {
//            @Override
//            public void onBarcodeRead(String barcode) {
//                isUsingBarcodeScanner = true;
//                prodCode.setText(barcode);
//                productInfo = cashier.getProductInfo(barcode);
//                barcode2.setText(productInfo.get(0));
//                prodName2.setText(productInfo.get(1));
//                desc2.setText(productInfo.get(3));
//                quantity2.setText(productInfo.get(4));
//                qty.requestFocus();
//                qty.setText("1");
//                isFocus = true;
//            }
//        });
//    }
//    
//    private void newTransaction(){
//        model.getDataVector().removeAllElements();
//        model.fireTableDataChanged();
//        int transNo = Integer.parseInt(transactionNumber.getText());
//        transNo++;
//        transactionNumber.setText(String.valueOf(transNo));
//        generateInvoiceId();
//        refreshItemTable();
//    }
//    
    private void createKeybindings(JTable table) {
        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        table.getActionMap().put("Enter", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(ae.getSource() == productTable){
                    cashier.setSearch(false);
                    String barcode = productTable.getValueAt(cashier.getSelectedRow(), 0).toString();
                    searchProduct.setText(barcode);
                    switchToPanel(mainPanel, panel1);
                    quantity.requestFocusInWindow();
                }
                else if(ae.getSource() == pendingTable) {
                    pending = true;
                    String invId = pendingTable.getValueAt(pendingTable.getSelectedRow(), 1).toString();
                    String subTotal = pendingTable.getValueAt(pendingTable.getSelectedRow(), 4).toString();
                    String custId = pendingTable.getValueAt(pendingTable.getSelectedRow(), 3).toString();
                    customerId.setText(custId);
                    customerName.setText(cashier.getCustomerName(custId));
                    customerPoints.setText(String.valueOf(cashier.getCustomerPoints(custId)));
                    invoiceId.setText(invId);
                    cashier.loadOrder(transTable, invId);
                    switchToPanel(mainTransactionPanel, transactionPanel);
                    total.setText(subTotal);
                    searchProduct.setText("");
                    searchProduct.requestFocusInWindow();
                }
            }
        });
    }
    
    private void clearFields(){
        save = false;
        searchProduct.setText(null);
        quantity.setText("1");
        searchProduct.requestFocusInWindow();
        
    }

    private void generateInvoiceId(){
        String uniqueID = UUID.randomUUID().toString().substring(26);
        invoiceId.setText(uniqueID);
    }

    private void clearTransaction() {
        customerId.setText("");
        customerName.setText("");
        customerPoints.setText("");

        barcode1.setText("");
        prodName1.setText("");
        desc1.setText("");
        qty1.setText("");
        total.setText("P0.0");

        quantity.setText("1");
        searchProduct.setText("");
        searchProduct.requestFocus();

        inputAmount1.setText("");
        changeValue1.setText("");

        tot = 0;

        switchToPanel(main1, amt1);

        CustomTableModel.transactionTableModel.getDataVector().removeAllElements();
        CustomTableModel.transactionTableModel.fireTableDataChanged();
    }
    
    private void save(){
        String value = total.getText();
        double tot = Double.parseDouble(value.substring(1, value.length()));
        double cash = Double.parseDouble(inputAmount1.getText());
        String value1 = changeValue1.getText();
        double change = Double.parseDouble(value1.substring(1, value1.length()));
        cashier.saveTransaction(invoiceId.getText(), cashierId.getText(), customerId.getText(), tot, cash, change);

        for(int row = 0; row < transTable.getRowCount(); row++){
            String barcode = transTable.getValueAt(row, 1).toString();
            String prodName = transTable.getValueAt(row, 2).toString();
            String brandName = transTable.getValueAt(row, 3).toString();
            value = transTable.getValueAt(row, 4).toString();
            double price = Double.parseDouble(value.substring(1, value.length()));
            int quantity = Integer.parseInt(transTable.getValueAt(row, 5).toString());
            value1 = transTable.getValueAt(row, 6).toString();
            double subTotal = Double.parseDouble(value1.substring(1, value1.length()));
            cashier.saveOrder(invoiceId.getText(), barcode, prodName, brandName, price, quantity, subTotal);
        }
    }
    
    private void clearAll(){
        clearTransaction();

        generateInvoiceId();
        int number = cashier.getTotalTransaction(cashierId.getText());
        number++;
        transNo.setText(String.valueOf(number++));
    }
    
    private void pending(){
        if (transTable.getRowCount() != 0) {
            String value = total.getText();
            double tot = Double.parseDouble(value.substring(1, value.length()));
            cashier.pendingTransaction(invoiceId.getText(), cashierId.getText(), customerId.getText(), tot);
            
            for (int row = 0; row < transTable.getRowCount(); row++) {
                String barcode = transTable.getValueAt(row, 1).toString();
                String prodName = transTable.getValueAt(row, 2).toString();
                String brandName = transTable.getValueAt(row, 3).toString();
                value = transTable.getValueAt(row, 4).toString();
                double price = Double.parseDouble(value.substring(1, value.length()));
                int quantity = Integer.parseInt(transTable.getValueAt(row, 5).toString());
                String value1 = transTable.getValueAt(row, 6).toString();
                double subTotal = Double.parseDouble(value1.substring(1, value1.length()));
                cashier.saveOrder(invoiceId.getText(), barcode, prodName, brandName, price, quantity, subTotal);
            }
            
            clearTransaction();
            
            generateInvoiceId();
            int number = cashier.getTotalTransaction(cashierId.getText());
            number++;
            System.out.println(number);
            transNo.setText(String.valueOf(number++));
        }
    }
    
    public void setName(String empName, String id){
        name.setText(empName);
        cashierId.setText(id);
        int number = cashier.getTotalTransaction(cashierId.getText());
        number++;
        transNo.setText(String.valueOf(number));
    }
    
    public void clearRegisterForm(){
        registerId.setText("");
        registerPassword.setText("");
        registerPassword1.setText("");
    }
    
    public void clearLoginForm(){
        loginId.setText("");
        loginPass.setText("");
    }

    private void login() {
        String user = loginId.getText();
        String pass = loginPass.getText();
        
        if(!user.isEmpty() && !pass.isEmpty()){
            int result = auth.checkCashierLogin(user);
            System.out.println(result);
            if(result == 1){
                if (passwordEncryptor.checkPassword(pass, auth.getPassword(user))) {
                    setName(auth.getEmployeeName(user), user);
                    login.setVisible(false);
                    authenticate = false;
                }
                else{
                    popUp.showMessage("ERROR: Password is incorrect!", 1);
                }
            }
            else{
            	popUp.showMessage("ERROR: Id is invalid!", 1);
            }
        }
        else{
            popUp.showMessage("ERROR: Some fields are empty!", 1);
        }
    }
    
    private void register(){
        String id = registerId.getText();
        String pass = registerPassword.getText();
        String pass1 = registerPassword1.getText();
        
        int result = auth.checkRegisterId(id);
        if (!id.isEmpty() && !pass.isEmpty() && !pass1.isEmpty()) {
            if (result == 1) {
                if (pass.equals(pass1)) {
                    int check = auth.addAccount(id, passwordEncryptor.encryptPassword(pass));
                    if (check == 0) {
                        switchToPanel(loginMain, loginPanel);
                        clearLoginForm();
                        clearRegisterForm();
                        popUp.showMessage("Account has been registered successfully!", 0);
                    } else {
                        popUp.showMessage("ERROR: Account was already registered!", 1);
                    }
                } else {
                        popUp.showMessage("ERROR: Password mismatched!", 1);
                }
            } else {
                popUp.showMessage("ERROR: Id is invalid!", 1);
            }
        }
        else{
            popUp.showMessage("ERROR: Some fields are empty!", 1);
        }
    }

    private void updateQuantity() {
        if(!add.getText().isEmpty() || !minus.getText().isEmpty()){
            int curQty = Integer.parseInt(transTable.getValueAt(row, 5).toString());
            int qty = 0;
            int newVal = 0;
            if(add.getText().isEmpty()){
                if(qty >= curQty){
                    popUp.showMessage("ERROR: Value exceeded the current quantity", 1);
                }
                else if(curQty <= qty){
                    popUp.showMessage("ERROR: Ordered product must atleast have a quantity of 1 or more than!", 1);
                }
                else{
                    qty = Integer.parseInt(minus.getText());
                    int totalQty = curQty - qty;
                    String price = transTable.getValueAt(row, 4).toString();
                    double subTotal = Double.parseDouble(price.substring(1, price.length())) * totalQty;  
                    transTable.setEditable(true);
                    transTable.setValueAt(totalQty, row, 5);
                    transTable.setValueAt("P" + subTotal, row, 6);
                    transTable.setEditable(false);
                    
                    double t = cashier.getProductPrice() * qty;
                    tot = tot + t;
                    total.setText("P" + tot);
                    popUp.showMessage("SUCCESS: Quantity has been updated!", 0);
                }
                
            }
            else{
                qty = Integer.parseInt(add.getText());
                int totalQty = curQty + qty;
                String price = transTable.getValueAt(row, 4).toString();
                double subTotal = Double.parseDouble(price.substring(1, price.length())) * totalQty;                            
                transTable.setEditable(true);
                System.out.println("Row: "+row+" Total: "+totalQty);
                transTable.setValueAt(totalQty, row, 5);
                transTable.setValueAt("P" + subTotal, row, 6);
                transTable.setEditable(false);
                
                double t = cashier.getProductPrice() * qty;
                tot = tot + t;
                total.setText("P" + tot);
                popUp.showMessage("SUCCESS: Quantity has been updated!", 0);
            }
            updateQty.setVisible(false);
        }
        else{
            popUp.showMessage("ERROR: Atleast one of the field must not be empty!", 1);
        }
    }
    
    public void deleteApprove(){
        String pinPassword = String.valueOf(pinPass.getText());
        String supervisorID = "sup10011";
        String supervisorID2 = "sup10012";
        if(!pinPassword.isEmpty()){
            if(pinPassword.equals(cashier.getSupervisorPinNumber(supervisorID)) || pinPassword.equals(cashier.getSupervisorPinNumber(supervisorID2))){   
                supervisorLoginFrame.setVisible(false);
                if(transTable.getSelectedRow() != -1){
                tot = 0;
                CustomTableModel.transactionTableModel.removeRow(transTable.getSelectedRow());
                    for(int i = 0; i < transTable.getRowCount(); i++){
                        String value = transTable.getValueAt(i, 6).toString();
                        tot = tot + Double.parseDouble(value.substring(1, value.length()));
                        transTable.setEditable(true);
                        transTable.setValueAt((i + 1), i, 0);
                        transTable.setEditable(false);
                    }
                total.setText("P"+String.valueOf(tot));
                searchProduct.requestFocusInWindow();
                popUp.showMessage("Delete Success!", 0);
        }
            }else{
                popUp.showMessage("ERROR: Pin Number is incorrect!", 1);
            }
        }else{
            popUp.showMessage("ERROR: Fields is empty!", 1);
        }
    }
    
    public void quantityDeleteApproval(){
        String pinPassword = String.valueOf(pinPassQuan.getText());
        String supervisorID1 = "sup10011";
        String supervisorID2 = "sup10012";
        if(!pinPassword.isEmpty()){
            if(pinPassword.equals(cashier.getSupervisorPinNumber(supervisorID1)) || pinPassword.equals(cashier.getSupervisorPinNumber(supervisorID2))){
                supervisorLoginFrameQuantity.setVisible(false);
                updateQuantity();
            }else{
                popUp.showMessage("ERROR: Pin Number is incorrect!", 1);
            }
        }else{
            popUp.showMessage("ERROR: Fields is empty!", 1);
        }
    }
}
